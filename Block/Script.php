<?php

namespace Tapbuy\Checkout\Block;

class Script extends \Magento\Framework\View\Element\Template
{
    protected $_helper;
    protected $_customerSession;
    protected $_checkoutSession;
    protected $_encryptedDatas = null;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Tapbuy\Checkout\Helper\Data $helper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Tapbuy\Checkout\Helper\Data $helper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
    }

    /**
     * Check if Tapbuy is activated
     * @return bool|mixed
     */
    public function isTapbuyEnabled()
    {
        $isTapbuyEnabled = $this->_helper->getConfig('tapbuy_checkout/general/enabled');
        if ($isTapbuyEnabled) {
            $this->_initEncryptedDatas();
        }

        return $isTapbuyEnabled;
    }

    /**
     * Encrypt datas with customer and cart informations
     */
    protected function _initEncryptedDatas()
    {
        $encryptionKey = $this->_helper->getConfig('tapbuy_checkout/general/encryption_key');
        $tapbuyParams = [];

        // check for current quote
        $quote = $this->_checkoutSession->getQuote();
        if ($quote && $quote->getItemsCount() > 0) {
            $tapbuyParams['id_cart'] = $quote->getId();
        }

        // check for current logged customer
        if ($this->_customerSession->isLoggedIn()) {
            $customer = $this->_customerSession->getCustomer();
            $tapbuyParams['id_customer'] = $customer->getId();
        }

        if (!empty($tapbuyParams)) {
            $tapbuyParamsJson = \Zend_Json::encode($tapbuyParams);

            if (class_exists('\phpseclib3\Crypt\AES')) {
                $aes = new \phpseclib3\Crypt\AES('ecb');
                $aes->setKeyLength(256);
            } else {
                $aes = new \phpseclib\Crypt\AES();
                $aes->setKeyLength(128);
            }

            $aes->setKey($encryptionKey);
            $this->_encryptedDatas = base64_encode($aes->encrypt($tapbuyParamsJson));
        }
    }

    /**
     * Returns the full Tapbuy script URL
     * @param bool $withKey
     * @return mixed|string
     */
    public function getScriptUrl($withKey = true)
    {
        $scriptUrl = $this->_helper->getConfig('tapbuy_checkout/general/script_url');
        if ($withKey && $this->_encryptedDatas) {
            $scriptUrl .= '?' . http_build_query(['key' => $this->_encryptedDatas]);
        }
        return $scriptUrl;

    }

    /**
     * Returns the Tapbuy key
     * @return mixed|string
     */
    public function getTapbuyKey()
    {
        return $this->_encryptedDatas;
    }
}
