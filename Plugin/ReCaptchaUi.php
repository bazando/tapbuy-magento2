<?php

namespace Tapbuy\Checkout\Plugin;

use Magento\ReCaptchaUi\Model\IsCaptchaEnabled;

class ReCaptchaUi
{
    protected $_tapbuyHelper;

    public function __construct(
        \Tapbuy\Checkout\Helper\Data $tapbuyHelper
    ) {
        $this->_tapbuyHelper = $tapbuyHelper;
    }

    public function afterIsCaptchaEnabledFor(
        IsCaptchaEnabled $subject,
        bool $result
    ): bool {
        if ($this->_tapbuyHelper->getConfig('tapbuy_checkout/general/retailer_name') == 'viapresse') {
            $result = false;
        }

        return $result;
    }
}
