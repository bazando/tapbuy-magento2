<?php

namespace Tapbuy\Checkout\Plugin;

use Sentry;

class BeforeMaskExceptionPlugin
{
    public function beforeMaskException(\Magento\Framework\Webapi\ErrorProcessor $subject, \Exception $exception)
    {
        Sentry\captureException($exception);

        return [$exception];
    }
}
