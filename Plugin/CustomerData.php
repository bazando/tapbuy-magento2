<?php

namespace Tapbuy\Checkout\Plugin;

class CustomerData
{
    protected $_tapbuyScript;
    protected $_resultPageFactory;
    protected $_layoutFactory;

    public function __construct(
        \Tapbuy\Checkout\Block\Script $tapbuyScript,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\View\Result\LayoutFactory $layoutFactory
    ) {
        $this->_tapbuyScript = $tapbuyScript;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_layoutFactory = $layoutFactory;
    }

    public function afterGetSectionData(\Magento\Checkout\CustomerData\Cart $subject, $result)
    {
        $tapbuyKey = ($this->_tapbuyScript->isTapbuyEnabled() ? $this->_tapbuyScript->getTapbuyKey() : null);
        $result['tapbuy_key'] = $tapbuyKey;
        $result['tapbuy_script_url'] = $this->_tapbuyScript->getScriptUrl();

        return $result;
    }
}
