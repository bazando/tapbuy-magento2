<?php

namespace Tapbuy\Checkout\Plugin;

use Sentry;
use Sentry\State\Scope;
use Tapbuy\Checkout\Model\Sentry\SentryPerformance;
use Magento\Framework\App\RequestInterface;

class AfterApiRequestPlugin
{
    /** @var SentryPerformance */
    private $sentryPerformance;

    /**
     * @var RequestInterface
     */
    private $request;
   
    /**
     * Constructor
     *
     * @param SentryPerformance $sentryPerformance
     * @param RequestInterface $request
     */
    public function __construct(SentryPerformance $sentryPerformance, RequestInterface $request)
    {
        $this->sentryPerformance = $sentryPerformance;
        $this->request = $request;
    }
    
    public function afterDispatch(\Magento\Webapi\Controller\Rest $subject, $result)
    {
        if ($this->request->getHeader('X-Tapbuy-Call')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $tapbuyHelper = $objectManager->create('Tapbuy\Checkout\Helper\Data');
            $sentryDsn = $tapbuyHelper->getConfig('tapbuy_checkout/general/sentry_dsn');
            $performanceEnabled = $tapbuyHelper->getConfig('tapbuy_checkout/general/sentry_performance_enabled');
            if (isset($sentryDsn) && !empty($sentryDsn) && $performanceEnabled) {
                $this->sentryPerformance->finishTransaction($result);
            }
        }

        return $result;
    }
}
