<?php

namespace Tapbuy\Checkout\Plugin;

use Sentry;
use Sentry\State\Scope;
use Tapbuy\Checkout\Model\Sentry\SentryPerformance;

class BeforeApiRequestPlugin
{
    /** @var SentryPerformance */
    private $sentryPerformance;


    public function __construct(SentryPerformance $sentryPerformance)
    {
        $this->sentryPerformance = $sentryPerformance;
    }
    
    public function beforeDispatch(\Magento\Webapi\Controller\Rest $subject, \Magento\Framework\App\RequestInterface $request)
    {
        if ($request->getHeader('X-Tapbuy-Call')) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $tapbuyHelper = $objectManager->create('Tapbuy\Checkout\Helper\Data');
            $sentryDsn = $tapbuyHelper->getConfig('tapbuy_checkout/general/sentry_dsn');
            $retailerName = $tapbuyHelper->getConfig('tapbuy_checkout/general/retailer_name');
            $performanceEnabled = $tapbuyHelper->getConfig('tapbuy_checkout/general/sentry_performance_enabled');
            if (isset($sentryDsn) && !empty($sentryDsn)) {
                Sentry\init([
                    'dsn' => $sentryDsn,
                    'traces_sample_rate' => 1.0,
                    'profiles_sample_rate' => 1.0,
                    'before_send' => function (Sentry\Event $event, ?Sentry\EventHint $hint): ?Sentry\Event {
                        if (null !== $hint && isset($hint->exception)) {
                            $exception = $hint->exception;
                            
                            if (strpos($exception->getMessage(), "Internal Error. Details are available in Magento log file") !== false) {
                                return null;
                            }
                        }
                
                        return $event;
                    },
                ]);
                Sentry\configureScope(function (Scope $scope) use ($retailerName) {
                    $scope->setTag('magento.retailer.name', $retailerName);
                });

                if ($request->getHeader('Sentry-Trace')) {
                    Sentry\continueTrace($request->getHeader('Sentry-Trace'), $request->getHeader('Baggage'));
                }

                if ($performanceEnabled) {
                    $this->sentryPerformance->startTransaction($request);
                }
            }
        }
    }
}
