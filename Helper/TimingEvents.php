<?php
namespace Tapbuy\Checkout\Helper;

class TimingEvents {
    private $timings = [];
    private $events = [];

    // Place a start marker
    public function start($name) {
        $this->events[$name] = microtime(true);
    }

    // Place an end marker and calculate the duration
    public function end($name) {
        if (isset($this->events[$name])) {
            $startTime = $this->events[$name];
            $endTime = microtime(true);
            $duration = $endTime - $startTime;
            $this->timings[] = [
                'name' => $name,
                'duration' => $duration,
                'start' => $startTime,
                'end' => $endTime,
            ];
            unset($this->events[$name]);
        } else {
            throw new \Exception("No start time found for $name");
        }
    }

    // Get the timings sorted by order of appearance
    public function getTimings() {
        return $this->timings;
    }
}
?>