define([
    "jquery",
    "Magento_Customer/js/customer-data"
], function($, customerData) {
    "use strict";

    function main(config, element) {
        var cart = customerData.get('cart')();
        updateTapbuy(cart);
    };

    $(document).ajaxComplete(function (event, xhr, settings) {
        if (settings.url.indexOf("customer/section/load/") > 0) {
            var cartObj = xhr.responseJSON;
            if (typeof cartObj.cart != 'undefined') {
                updateTapbuy(cartObj.cart);
            }
        }
    });

    function updateTapbuy(cart) {
        var tapbuyKey = (typeof cart.tapbuy_key != 'undefined' ? cart.tapbuy_key : null);
        if (tapbuyKey !== null) {
            if (typeof Tapbuy == 'object') {
                Tapbuy.enabled = true;
                Tapbuy.key = tapbuyKey;
            }
            else {
                var tapbuyScript = document.createElement('script');
                tapbuyScript.type = 'text/javascript';
                tapbuyScript.async = true;
                tapbuyScript.onload = function() {
                    if (typeof TapbuyInit == 'function') {
                        TapbuyInit();
                    }
                };
                tapbuyScript.src = cart.tapbuy_script_url;
                document.getElementsByTagName('head')[0].appendChild(tapbuyScript);
            }
        }
        else if (typeof Tapbuy == 'object') {
            Tapbuy.enabled = false;
        }
    }

    return main;
});