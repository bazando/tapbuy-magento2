<?php

namespace Tapbuy\Checkout\Model;

use Tapbuy\Checkout\Api\AttributeInterface;
use Tapbuy\Checkout\Api\Data\TapbuyAttribute;
use Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption;

class Attribute implements AttributeInterface
{
    public function gettAttribute($attributeName)
    {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        // attribute data
        $attributeInterceptor = $objectManager->create('Magento\Catalog\Model\Product\Attribute\Repository\Interceptor');
        $attribute = $attributeInterceptor->get($attributeName);

        $tapbuyAttribute = new TapbuyAttribute();
        $tapbuyAttribute->setAttribute($attribute);

        // get option list
        $options = $attribute->getSource()->getAllOptions();

        // get attributes ID to  get options data
        $valueIds = [];
        foreach ($options as $option) {
            $valueIds[] = $option['value'];
        }

        // get swatches option data
        $dataHelper = $objectManager->create('Magento\Swatches\Helper\Data');
        $swatchOptions = $dataHelper->getSwatchesByOptionsId($valueIds);

        $swatchHelper=$objectManager->create("Magento\Swatches\Helper\Media");

        // build the structured data we want to return
        foreach ($options as $option) {

            $extendedOption = new TapbuyAttributeExtendedOption($option['label'], $option['value'], null, null, null, null);

            if(isset($swatchOptions[$option['value']])) {

                if($swatchOptions[$option['value']]['type'] == 1) {
                    // it's a hexacode
                    $extendedOption->setHexCode($swatchOptions[$option['value']]['value']);

                } elseif($swatchOptions[$option['value']]['type'] == 2) {
                    // it's an image
                    $extendedOption->setImagePath($swatchOptions[$option['value']]['value']);

                    // get image URLs
                    $ThumbImage =  $swatchHelper->getSwatchAttributeImage('swatch_thumb', $swatchOptions[$option['value']]['value']);
                    $SwatchImage = $swatchHelper->getSwatchAttributeImage('swatch_image', $swatchOptions[$option['value']]['value']);

                    $extendedOption->setImageThumb($ThumbImage);
                    $extendedOption->setImage($SwatchImage);

                }

            }

            $tapbuyAttribute->addExtendedOption($extendedOption);

        }

        return $tapbuyAttribute;
    }
}
