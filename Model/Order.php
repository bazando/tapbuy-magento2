<?php

namespace Tapbuy\Checkout\Model;

use Magento\Sales\Api\Data\OrderStatusHistoryInterface;
use Tapbuy\Checkout\Api\OrderInterface;

class Order implements OrderInterface
{
    /**
     *
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;

    /**
     *
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(\Magento\Framework\Webapi\Rest\Request $request)
    {
        $this->_request = $request;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\OrderInterface::addComment()
     */
    public function addComment($orderId, OrderStatusHistoryInterface $statusHistory)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $orderRepository = $objectManager->create('Magento\Sales\Model\OrderRepository');
        $order = $orderRepository->get($orderId);

        if ($order->getId()) {
            $statusHistory->setStatus($order->getStatus());
            $order->setStatusHistories(array_merge($order->getStatusHistories(), [$statusHistory]));
            $order->setDataChanges(true);
            $orderRepository->save($order);
        }

        return $order;
    }

    /**
     * Manage the return of Paypal Express Checkout
     * @param string $orderExtId
     */
    public function expressCheckoutReturn($orderExtId)
    {
        try {
            $bodyParams = $this->_request->getBodyParams();

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $collection = $objectManager->create('Magento\Sales\Model\Order');
            $orderInfo = $collection->loadByIncrementId($orderExtId);
            $orderId = $orderInfo->getId();
            $orderRepository = $objectManager->create('Magento\Sales\Model\OrderRepository');
            $order = $orderRepository->get($orderId);

            $configParameters = [
                'params' => [
                    \Magento\Paypal\Model\Config::METHOD_WPP_EXPRESS,
                    $order->getStoreId()
                ]
            ];

            $paypalConfigFactory = $objectManager->create('Magento\Paypal\Model\ConfigFactory');
            $paypalConfig = $paypalConfigFactory->create($configParameters);
            $paypalConfig->setMethodInstance($order->getPayment()->getMethodInstance());

            $paypalApiFactory = $objectManager->create('Magento\Paypal\Model\Api\Type\Factory');
            $api = $paypalApiFactory->create(\Magento\Paypal\Model\Api\Nvp::class)
                ->setConfigObject($paypalConfig)
                ->setToken($bodyParams['payment_data']['token']);

            $api->callGetExpressCheckoutDetails();

            $order->getPayment()->setAdditionalInformation(\Magento\Paypal\Model\Express\Checkout::PAYMENT_INFO_TRANSPORT_PAYER_ID, $api->getPayerId())
                ->setAdditionalInformation(\Magento\Paypal\Model\Express\Checkout::PAYMENT_INFO_TRANSPORT_TOKEN, $api->getToken());

            $paymentMethodInstance = $order->getPayment()->getMethodInstance();
            $paymentMethodInstance->order($order->getPayment(), round((float)$order->getBaseGrandTotal(), 2));

        } catch (\Exception $e) {

            throw new \Exception('No method provided', 404);
        }
    }

}
