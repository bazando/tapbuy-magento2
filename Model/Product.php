<?php

namespace Tapbuy\Checkout\Model;

use Tapbuy\Checkout\Api\ProductInterface;

class Product implements ProductInterface
{

    private function getRetailerName()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $tapbuyHelper = $objectManager->create('Tapbuy\Checkout\Helper\Data');
        return $tapbuyHelper->getConfig('tapbuy_checkout/general/retailer_name');
    }
    /**
     * {@inheritdoc}
     */
    public function getProduct($sku)
    {
        // get product interceptor
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $interceptor = $objectManager->create('Magento\Catalog\Model\ProductRepository\Interceptor');
        // get product from theinterceptor
        $product = $interceptor->get($sku);

        // get product type
        $type_id = $product->getTypeId();

        // get children for the current objet or the parent object, we need them to calculate the product configuration attributes in Tapbuy PWA
        $parentSku = null;
        $childrenProducts = [];
        $parentProduct = null;
        if ($type_id != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
            $parentProductId = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getId());

            if (!empty($parentProductId) || isset($parentProductId[0])) {
                try {
                    $parentProductId = $parentProductId[0];
                    $parentProduct   = $interceptor->getById($parentProductId);
                    $parentSku       = $parentProduct->getSku();
                } catch (\Exception $e) {
                    // nothing to do, the parent product was not found
                }
            }
        } else {
            $parentSku = $product->getSku();
        }

        if (!is_null($parentSku)) {
            $childrenProducts = $objectManager->create('\Magento\ConfigurableProduct\Model\LinkManagement')->getChildren($parentSku);
        }

        // set the object to return by the API
        $tapbuyProduct = new \Tapbuy\Checkout\Api\Data\TapbuyProduct();
        $tapbuyProduct->setProduct($product);
        $tapbuyProduct->setChildrenProducts($childrenProducts);
        $tapbuyProduct->setParentProduct($parentProduct);

        if ($this->getRetailerName() == 'viapresse') {
            $viapresseProduct = $objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseProduct');
            $tapbuyProduct = $viapresseProduct->setProductCoverImage($product, $tapbuyProduct);
        }

        return $tapbuyProduct;
    }
}
