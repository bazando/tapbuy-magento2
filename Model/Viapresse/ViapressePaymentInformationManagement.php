<?php

namespace Tapbuy\Checkout\Model\Viapresse;

use Tapbuy\Checkout\Api\Viapresse\PaymentInformationManagementInterface;

class ViapressePaymentInformationManagement implements PaymentInformationManagementInterface
{
    /**
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;

    /**
     * @var  \Magento\Framework\App\ObjectManager
     */
    protected $objectManager;

    /**
     *
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(\Magento\Framework\Webapi\Rest\Request $request)
    {
        $this->_request = $request;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    private function shouldExcludePaymentMethodsSoundkit($quote): bool
    {
        $isSoundKitProduct = $this->objectManager->create('Viapresse\SoundKit\Service\IsSoundKitProduct');
        foreach ($quote->getAllItems() as $item) {
            if ($isSoundKitProduct->check($item->getSku())) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getList($quoteId)
    {
        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');
        $quote = $quoteRepository->get($quoteId);
        $methodList = $this->objectManager->create('\Magento\Payment\Model\MethodList');
        $availableMethods = $methodList->getAvailableMethods($quote);
        $configFactory = $this->objectManager->create('\Viapresse\Payment\Api\ConfigInterface');

        $storeId = $quote->getStoreId();

        $defaultSepaPaymentMethod = $configFactory->getDefaultSepaPaymentMethod($storeId);
        $adlPaymentMethods = $configFactory->getAdlPaymentMethods($storeId);
        $mixedCartPaymentMethods = $configFactory->getMixedCartPaymentMethods($storeId);
        $excludePaymentMethodsForSoundKit = $this->shouldExcludePaymentMethodsSoundKit($quote);
        $methodsResponse = [];
        $zeroTotalCheck = $this->objectManager->create('Magento\Payment\Model\Checks\ZeroTotal');
        $extentionAttributes = $quote->getExtensionAttributes();

        foreach ($availableMethods as $paymentMethod) {
            $append = false;
            $isZeroTotalApplicable = $zeroTotalCheck->isApplicable($paymentMethod, $quote);

            if ($isZeroTotalApplicable) {
                if ($extentionAttributes->getHasOnlyAdl() && in_array($paymentMethod->getCode(), $adlPaymentMethods)) {
                    $append = true;
                }
            }

            if ($extentionAttributes->getHasAdl()) {
                if ($extentionAttributes->getHasOnlyAdl() && in_array($paymentMethod->getCode(), $adlPaymentMethods)) {
                    $append = true;
                }

                if (!$extentionAttributes->getHasOnlyAdl() && in_array($paymentMethod->getCode(), $mixedCartPaymentMethods)) {
                    $append = true;
                }
            } elseif ($paymentMethod->getCode() !== $defaultSepaPaymentMethod) {
                $append = true;
            }

            if ($paymentMethod->getCode() === 'hipay_hosted_fields') {
                $append = false;
            }

            if ($append) {
                $methodsResponse[] = $paymentMethod;
            }
        }

        // @todo: remove this check after migration to new DiscoverPress plugin
        // see VIAP-75 for more details
        if (class_exists('Viapresse\GiftProduct\Model\Payment\Checks\DiscoverPress')) {
            $discoverPressChecks = $this->objectManager->create('Viapresse\GiftProduct\Model\Payment\Checks\DiscoverPress');
            foreach ($methodsResponse as $key => $method) {
                if ($method->getCode() == 'free') {
                    continue;
                }
                if (!$discoverPressChecks->isApplicable($method, $quote)) {
                    unset($methodsResponse[$key]);
                }
            }
        } else {
            $viaGiftProductHelperConfig = $this->objectManager->create('Viapresse\GiftProduct\Helper\Config');
            foreach ($methodsResponse as $key => $method) {
                if ($method->getCode() == 'free') {
                    continue;
                }
                if ($quote->getExtensionAttributes()->getHasDiscoverPress()) {
                    if (!in_array($paymentMethod->getCode(), $viaGiftProductHelperConfig->getAllowedPaymentMethods())) {
                        unset($methodsResponse[$key]);
                    }
                }
            }
        }

        if ($excludePaymentMethodsForSoundKit) {

            $helperSoundKit = $this->objectManager->create('Viapresse\SoundKit\Helper\Data');

            foreach ($methodsResponse as $key => $method) {
                if ($method->getCode() == 'free') {
                    continue;
                }
                if (!in_array(
                    $method->getCode(),
                    $helperSoundKit->getAllowedPaymentMethodsForSoundKit($storeId),
                    true
                )) {
                    unset($methodsResponse[$key]);
                }
            }
        }

        $subscriptionConfig = $this->objectManager->create('Viapresse\Subscription\Api\ConfigInterface');

        foreach ($methodsResponse as $key => $method) {
            if ((int)$quote->getData('is_subscription_adl_extension') == 1
                && !in_array($method->getCode(), $subscriptionConfig->getAdlExtenstionAllowedPaymentMethods(null))) {
                unset($methodsResponse[$key]);
            }
        }

        return $methodsResponse;
    }
}
