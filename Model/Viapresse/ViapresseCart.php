<?php

namespace Tapbuy\Checkout\Model\Viapresse;
use Magento\Framework\App\Area;

class ViapresseCart
{
    private $objectManager;
    private $timingEvents;

    public function __construct()
    {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->timingEvents = new \Tapbuy\Checkout\Helper\TimingEvents();
    }

    public function setTimingEvents($timingEvents)
    {
        $this->timingEvents = $timingEvents;
    }

    /**
     * Set missing product cover images
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return void
    */
    public function setCoverImage($item, $additionalData = [], $product = null)
    {
        $viaGiftHelper = $this->objectManager->create('Viapresse\GiftProduct\ViewModel\GiftProduct');
        $result = [];

        // Is Gift Card
        if ($viaGiftHelper->isGiftcardProduct($product->getSku())) {
            if (isset($additionalData['giftcard_options']) && isset($additionalData['giftcard_options']['card_model'])) {

                $quoteItemHelper = $this->objectManager->create('Viapresse\Quote\Helper\Item');
                $result['tapbuy_image'] = $quoteItemHelper->getGiftCardImageUrl($item);
            }
        }
        return $result;
    }

    /**
     * Set products urls in informations data
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return void
    */
    public function setProductUrl($item)
    {
        $result['tapbuy_url'] = $item->getRedirectUrl();

        return $result;
    }

    /**
     * Set products additional informations data
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return void
    */
    public function setProductDescriptions($item, $additionalData = [], $product = null)
    {

        // Emulate front to get the translation right
        $appEmulation = $this->objectManager->create('Magento\Store\Model\App\Emulation');
        $appEmulation->startEnvironmentEmulation($item->getQuote()->getStoreId(), Area::AREA_FRONTEND, true);

        $viapresseData = $this->objectManager->create('Viapresse\Catalog\Helper\Data');
        $viaGiftHelper = $this->objectManager->create('Viapresse\GiftProduct\ViewModel\GiftProduct');
        $isSoundKitProduct = $this->objectManager->create('Viapresse\SoundKit\Service\IsSoundKitProduct');

        $result = [];

        $arrayManager = $this->objectManager->create('Magento\Framework\Stdlib\ArrayManager');
        $offerType = $this->objectManager->create('Viapresse\Catalog\Model\Attribute\Source\OfferType');
        $attributeCode = $this->objectManager->create('Viapresse\Catalog\AttributeCode');
        $result['tapbuy_descriptions'] = [];

        if ($isSoundKitProduct->check($product->getSku())) {
            $result['tapbuy_descriptions']['full_name'][] = 'Les articles en audio';
        }

        // Is DiscoverPress gift product
        if ($viaGiftHelper->isGiftProduct($product->getSku())) {
            $result['tapbuy_descriptions']['full_name'][] = 'Essai gratuit pendant 2 mois, puis 0,99€/mois au lieu de 9,99€';
            $result['tapbuy_descriptions']['description'][] = 'Résiliable à tout moment';
        }

        // Is Gift Card
        if ($viaGiftHelper->isGiftcardProduct($product->getSku())) {
            $result['tapbuy_descriptions']['full_name'][] = 'Disponibilité immédiate.';
            $result['tapbuy_descriptions']['description'][] = 'Valable en une ou plusieurs fois';
        }

        if ($viapresseData->isOffer($product)) {
            switch ($arrayManager->get($attributeCode::VP_OFFER_TYPE, $additionalData)) {
                case $offerType::ADD:
                    $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_OFFER_SUBSCRIPTION_DURATION . '_label', $additionalData);
                    if ((int)$arrayManager->get($attributeCode::VP_OFFER_NB_ISSUES, $additionalData) > 0) {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_OFFER_NB_ISSUES, $additionalData) . ' n°';
                    }

                    if ($arrayManager->get($attributeCode::VP_ISSUE_MEDIUM, $additionalData) == $viapresseData->getPaperMediumOptionId()
                            && $arrayManager->get($attributeCode::VP_TITLE_FREE_DIGITAL_COUPLING, $additionalData)) {
                        $result['tapbuy_descriptions']['full_name'][] = sprintf('%s %s %s', $arrayManager->get($attributeCode::VP_ISSUE_MEDIUM . '_label', $additionalData), '+', __('free digital version'));
                    } else {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_ISSUE_MEDIUM . '_label', $additionalData);
                    }
                    break;
                case $offerType::ADL:
                    $result['tapbuy_descriptions']['full_name'][] = __('Free time: monthly direct debit');
                    if (!empty($arrayManager->get($attributeCode::VP_OFFER_ADDITIONAL_NAME, $additionalData))) {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_OFFER_ADDITIONAL_NAME, $additionalData);
                    }

                    if ($arrayManager->get($attributeCode::VP_ISSUE_MEDIUM, $additionalData) == $viapresseData->getPaperMediumOptionId()
                        && $arrayManager->get($attributeCode::VP_TITLE_FREE_DIGITAL_COUPLING, $additionalData)) {
                        $result['tapbuy_descriptions']['full_name'][] = sprintf('%s %s %s', $arrayManager->get($attributeCode::VP_ISSUE_MEDIUM . '_label', $additionalData), '+', __('free digital version'));
                    } else {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_ISSUE_MEDIUM . '_label', $additionalData);
                    }
                    $adlMessage = $viapresseData->getAdlMessage($product);
                    $result['tapbuy_descriptions']['description'][] = $adlMessage;
                    $result['tapbuy_descriptions']['description'][] = __('Start of direct debits after receipt of numbers');
                    break;
                case $offerType::VAN:
                    $result['tapbuy_descriptions']['full_name'][] = 'Achat à l\'unité du n°' . $arrayManager->get('vp_issue_number', $additionalData);

                    if (!empty($arrayManager->get($attributeCode::VP_OFFER_ADDITIONAL_NAME, $additionalData))) {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_OFFER_ADDITIONAL_NAME, $additionalData);
                    }

                    if ($arrayManager->get($attributeCode::VP_ISSUE_MEDIUM, $additionalData) == $viapresseData->getPaperMediumOptionId()
                        && $arrayManager->get($attributeCode::VP_TITLE_FREE_DIGITAL_COUPLING, $additionalData)) {
                        $result['tapbuy_descriptions']['full_name'][] = sprintf('%s %s %s', $arrayManager->get($attributeCode::VP_ISSUE_MEDIUM . '_label', $additionalData), '+', __('free digital version'));
                    } else {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_ISSUE_MEDIUM . '_label', $additionalData);
                    }
                    break;
                default:
                    $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_OFFER_SUBSCRIPTION_DURATION . '_label', $additionalData);
                    if ((int)$arrayManager->get($attributeCode::VP_OFFER_NB_ISSUES, $additionalData) > 0) {
                        $result['tapbuy_descriptions']['full_name'][] = $arrayManager->get($attributeCode::VP_OFFER_NB_ISSUES, $additionalData) . ' n°';
                    }
            }
        }

        foreach($item->getChildren() as $child) {
            if ($viapresseData->isGift($child->getProduct()) && !$viaGiftHelper->isGiftcardProduct($product->getSku())) {
                $result['tapbuy_descriptions']['description'][] = '+ ' . __('Gift option') . ' : ' . $child->getProduct()->getName();
            }
        }

        return $result;
    }

    /**
     * Set products additional data for Tapbuy
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
    */
    public function setItemsTapbuyData($quote)
    {
        foreach ($quote->getAllItems() as $item) {
            $additionalData = $item->getAdditionalData();

            $product = $item->getProduct();

            $productDescriptions = $this->setProductDescriptions($item, $additionalData, $product);
            $additionalData = array_merge($additionalData, $productDescriptions);

            $productUrl = $this->setProductUrl($item);
            $additionalData = array_merge($additionalData, $productUrl);

            $coverImage = $this->setCoverImage($item, $additionalData, $product);
            $additionalData = array_merge($additionalData, $coverImage);

            $item->setAdditionalData($additionalData);
        }
    }

    /**
     * Check if customer must keep gift product
     * See Viapresse\GiftProduct\Observer\QuoteMergeAfter->execute()
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
     */
    private function checkGiftProduct($quoteId, $customer)
    {
        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');
        // ! Bottleneck here (2.55%), we should try to pass the quote object instead of the quoteId
        $quote = $quoteRepository->get($quoteId);
        $isGiftProductService = $this->objectManager->create('Viapresse\GiftProduct\Service\IsGiftProduct');
        $giftProductAlreadyOffered = $this->objectManager->create('Viapresse\GiftProduct\Service\GiftProduct\GiftProductAlreadyOffered');
        $giftProductItems = [];
        foreach ($quote->getAllItems() as $item) {
            if ($isGiftProductService->check($item->getSku())) {
                // force quantity
                $item->setQty(1);
                $giftProductItems[] = $item;
            }
        }

        if (count($giftProductItems) >= 1) {
            // if customer hasn't a Gift product, keep one
            if (!$giftProductAlreadyOffered->productAlreadyOfferedByCustomer($customer)) {
                \array_shift($giftProductItems);
            }

            foreach ($giftProductItems as $item) {
                $quote->deleteItem($item);
            }
            // ! Bottleneck here (13.84%), we should try without saving the quote here
            $quoteRepository->save($quote);
        }
    }

    public function resetAddressForUnavailableCountry($quote)
    {
        $wrongCountry = false;
        $quoteRepository = $this->objectManager->create('Magento\Quote\Api\CartRepositoryInterface');
        $shippingMethod = 'viapresse_shipping_grid';
        $actualShippingAddress = $quote->getShippingAddress();
        $shippingAddress = $actualShippingAddress;
        if ($actualShippingAddress) {
            foreach ($quote->getItems() as $item) {
                $additionalData = $item->getAdditionalData();
                if (!empty($additionalData['via_shipping_grid']) && !empty($additionalData['via_shipping_grid']['rates'])) {
                    $wrongCountry = !in_array($actualShippingAddress->getCountryId(), array_keys($additionalData['via_shipping_grid']['rates']));
                    if ($wrongCountry) {
                        break;
                    }
                }
            }
            if ($wrongCountry) {
                $tapbuyCartModel = $this->objectManager->create('Tapbuy\Checkout\Model\Cart');
                $shippingAddress = $tapbuyCartModel->buildAddress([
                    'country_id' => 'FR',
                    'street' => [],
                    'postcode' => '',
                    'city' => '',
                    'same_as_billing' => 0,
                    // 'save_in_address_book' => 1,
                ]);
                $quote->removeAddress($actualShippingAddress->getId());
                $quote->setShippingAddress($shippingAddress);
                $address = $quote->getShippingAddress();
                foreach ($quote->getItems() as $item) {
                    $address->addItem($item);
                }
                $quote->getShippingAddress()->setShippingMethod($shippingMethod);
                $quoteRepository->save($quote);
            }
            $extentionAttributes = $quote->getExtensionAttributes();
            if ($extentionAttributes) {
                $shippingAssignments = $extentionAttributes->getShippingAssignments();
                if ($shippingAssignments) {
                    foreach ($shippingAssignments as $shippingAssignment) {
                        $shipping = $shippingAssignment->getShipping();
                        if ($shipping) {
                            $address = $shipping->getAddress();
                            foreach ($shippingAssignment->getItems() as $item) {
                                $shipping->setAddress($shippingAddress);
                                $shipping->setMethod($shippingMethod);
                            }
                        }
                    }
                }
            }
        }

        return $wrongCountry;
    }

    /**
     * Init cart for multishipping Viapresse
     * See Viapresse\Multishipping\Model\Checkout\Type\Multishipping->init()
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
     */
    private function initMultishipingCart($quoteId, $customer)
    {
        $addressRepository = $this->objectManager->create('Magento\Customer\Api\AddressRepositoryInterface');
        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');
        // ! Bottleneck here (3.77%), we should try to pass the quote object instead of the quoteId
        $quote = $quoteRepository->get($quoteId);
        $viapresseCutomerModel = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseCustomer');
        /**
         * Remove all addresses
         */
        $addresses = $quote->getAllAddresses();
        foreach ($addresses as $address) {
            $quote->removeAddress($address->getId());
        }

        $defaultShippingId = $viapresseCutomerModel->getCustomerDefaultShippingAddress($customer);
        if ($defaultShippingId) {
            $quote->getShippingAddress()->importCustomerAddressData(
                $addressRepository->getById($defaultShippingId)
            );

            $this->resetAddressForUnavailableCountry($quote);

            foreach ($quote->getItems() as $item) {
                /**
                 * Items with parent id we add in importQuoteItem method.
                 * Skip virtual items
                 */
                if ($item->getParentItemId() || $item->getProduct()->getIsVirtual()) {
                    continue;
                }
                $quote->getShippingAddress()->addItem($item);
            }
        }

        $defaultBillingAddressId = $viapresseCutomerModel->getCustomerDefaultBillingAddress($customer);
        if ($defaultBillingAddressId) {
            $quote->getBillingAddress()->importCustomerAddressData(
                $addressRepository->getById($defaultBillingAddressId)
            );
            foreach ($quote->getItems() as $item) {
                if ($item->getParentItemId()) {
                    continue;
                }
                if ($item->getProduct()->getIsVirtual()) {
                    $quote->getBillingAddress()->addItem($item);
                }
            }
        }
        // ! Bottleneck here (5.80%), we should try without saving the quote here
        $quoteRepository->save($quote);

        if (!$viapresseCutomerModel->getCustomerDefaultShippingAddress($customer)) {
            return;
        }

        $shippingMethod = 'viapresse_shipping_grid'; // @TODO do not hardcode this
        $quote
            ->setTriggerRecollect(1)
            ->getShippingAddress()
            ->setCustomerAddressId($viapresseCutomerModel->getCustomerDefaultShippingAddress($customer))
            ->setShippingMethod($shippingMethod);

        $cartExtension = $quote->getExtensionAttributes();
        if ($cartExtension === null) {
            $this->objectManager->create('Magento\Quote\Api\Data\CartExtensionFactory');
        }

        $shippingAssignment = $this->objectManager->get(\Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor::class)->create($quote);
        $shipping = $shippingAssignment->getShipping();

        $shippingAssignment->setShipping($shipping);
        $cartExtension->setShippingAssignments([$shippingAssignment]);

        $quote
        ->setExtensionAttributes($cartExtension)
        ->getShippingAddress()
        ->setCollectShippingRates(true)
        ->collectShippingRates();

        // ! Bottleneck here (5.25%), we should try without saving the quote here
        $quoteRepository->save($quote);
    }

    /**
     * Check cart contains gift product
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    private function hasGiftProduct($quote): bool
    {

        $viaGiftProductHelperConfigFactory = $this->objectManager->create('Viapresse\GiftProduct\Helper\Config');
        $giftProductSku = $viaGiftProductHelperConfigFactory->getSkuGiftProduct();
        if ($giftProductSku) {
            $isGiftProduct = $this->objectManager->create('Viapresse\GiftProduct\Service\IsGiftProduct');
            foreach ($quote->getAllItems() as $quoteItem) {
                if ($quoteItem->getSku() !== $viaGiftProductHelperConfigFactory->getSkuGiftCardProduct() &&
                    $isGiftProduct->check($quoteItem->getSku(), $giftProductSku)
                ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if cart has Soundkit
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    protected function hasSoundKit($quote)
    {
        $isSoundKitProduct = $this->objectManager->create('Viapresse\SoundKit\Service\IsSoundKitProduct');
        foreach ($quote->getAllItems() as $item) {
            $sku = $item->getSku();
            if ($isSoundKitProduct->check($sku)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get cart items and soundkits
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    protected function getSoundKits($quote)
    {
        $isSoundKitProduct = $this->objectManager->create('Viapresse\SoundKit\Service\IsSoundKitProduct');
        $quoteItems = [];
        foreach ($quote->getItems() as $item) {
            $quoteItems[] = $item;
            foreach ($item->getChildren() as $child) {
                $sku = $child->getSku();
                if ($isSoundKitProduct->check($sku)) {
                    $quoteItems[] = $child;
                }
            }
        }
        return $quoteItems;
    }

    /**
     * Get cart coupons
     * @param \Magento\Quote\Model\Quote $quote
     */
    protected function getCoupons($quote)
    {
        $viaCatalogHelperData = $this->objectManager->create('Viapresse\Catalog\Helper\Data');

        // Emulate front to get the translation right
        $appEmulation = $this->objectManager->create('Magento\Store\Model\App\Emulation');
        $appEmulation->startEnvironmentEmulation($quote->getStoreId(), Area::AREA_FRONTEND, true);

        $priceHelper = $this->objectManager->create('Magento\Framework\Pricing\Helper\Data');
        $productRepository = $this->objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
        $couponsHelper = $this->objectManager->create('Viapresse\Voucher\Block\Coupons');

        $coupons = [];
        $couponsTotal = 0;

        foreach ($quote->getItems() as $item) {
            foreach ($item->getChildren() as $child) {
                $product = $child->getProduct();
                if ($viaCatalogHelperData->isCoupon($product)) {
                    $couponValue = (float)$product->getData('vp_coupon_value');
                    $brand = $productRepository->getById($product->getData('vp_coupon_brand'));
                    $logo = $couponsHelper->getBrandLogoUrl($brand);
                    $accessory = new \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory();
                    $accessory->setId($child->getItemId());
                    $accessory->setName($product->getDescription());
                    $accessory->setPrice($couponValue);
                    $accessory->setImage($logo);
                    $coupons[] = $accessory;

                    $couponsTotal += $couponValue;
                }
            }
        }

        $accessories = null;

        if (count($coupons) > 0) {
            $accessories = new \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories();
            $accessories->setLabel(__('Included')->render());
            $accessories->setDescription($priceHelper->currency($couponsTotal, true, false) . ' ' . __('in vouchers'));
            $accessories->setItems($coupons);
        }

        return $accessories;
    }

    /**
     * Set shipping addresses items for Viapresse
     * DB table `quote_item` qty value can not be set to 1, if having more than 1 child references
     * in table `quote_address_item`.
     * @param string $quoteId
     * @return void
     */
    public function setShippingAddressesItems($quoteId)
    {

        // Setting the shipping assigments
        $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
        $quote = $quoteInterceptor->get($quoteId);
        $cartExtension = $quote->getExtensionAttributes();
        if ($cartExtension === null) {
            $this->objectManager->create('Magento\Quote\Api\Data\CartExtensionFactory');
        }

        $shippingAssignment = $this->objectManager->get(\Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor::class)->create($quote);
        $shipping = $shippingAssignment->getShipping();

        $shippingAssignment->setShipping($shipping);
        $cartExtension->setShippingAssignments([$shippingAssignment]);

        $quote->setExtensionAttributes($cartExtension)
        ->setTriggerRecollect(1)
        ->getShippingAddress()
        ->setCollectShippingRates(true)
        ->collectShippingRates();

        $quoteInterceptor->save($quote);

        // caveat. we need to get the quote again to get the correct values in 'getShippingAddressesItems()'
        $quote = $quoteInterceptor->get($quoteId);
        $quote->getShippingAddressesItems();
    }

    public function getCartBeforeTotals($request, $quoteId, $quote)
    {
        $this->setItemsTapbuyData($quote);
        if ($quote->getCustomerId()) {
            if (!$request->getHeader('X-Tapbuy-Skipmultishippinginit', false)) {
                // ! Bottleneck here (3.62%), find a way to optimize this
                $customer = $quote->getCustomer();
                // ! Bottleneck here (16.84%), find a way to optimize this

                $this->checkGiftProduct($quoteId, $customer);
                $quoteConnection = $this->objectManager->get(\Magento\Quote\Model\ResourceModel\Quote::class)->getConnection();

                $quoteConnection->beginTransaction();
                try {
                    // ! Bottleneck here (15.84%), find a way to optimize this
                    $this->initMultishipingCart($quoteId, $customer);
                } catch (\Exception $e) {
                    // do nothing
                }

                $quoteConnection->commit();
            }
        }
    }

    public function getCartAfterTotals($quote, $tapbuyCart)
    {
        $extentionAttributes = $quote->getExtensionAttributes();
        $tapbuyCart->setHasGiftProduct($this->hasGiftProduct($quote));
        $tapbuyCart->setHasRecurring($extentionAttributes->getHasAdl());
        $tapbuyCart->setHasOnlyRecurring($extentionAttributes->getHasOnlyAdl());
        $viapresseGiftOptions = $this->objectManager->create('\Tapbuy\Checkout\Model\Viapresse\ViapresseGiftOptions');
        if ($this->hasSoundKit($quote)) {
            $soundKits = $this->getSoundKits($quote);
            $tapbuyCart->getCart()->setItems($soundKits);
        }
        
        if ($viapresseGiftOptions->hasGiftOptions($quote)) {
            $items = $tapbuyCart->getCart()->getItems();
            $giftOptions = $viapresseGiftOptions->getGiftOptions($quote, $tapbuyCart);
            $mergedItems = []; // Final array with merged items

            foreach ($items as $item) {
                $mergedItems[] = $item; // Add the item to the merged array
                
                // Check if there’s a gift option for this item
                foreach ($giftOptions as $giftOption) {
                    if ($giftOption->getParentItemId() == $item->getItemId()) {
                        $mergedItems[] = $giftOption; // Add the gift option right after its parent item
                    }
                }
            }
            $tapbuyCart->getCart()->setItems($mergedItems);
        }
        $coupons = $this->getCoupons($quote);
        if ($coupons) {
            $tapbuyCart->setAccessories($coupons);
        }

        return $tapbuyCart;
    }

    public function updateProduct($origItem, $bodyParams)
    {
        $origItem->setMultishippingQty($bodyParams['qty']);

        if (!empty($bodyParams['additional_data'])) {
            $originAdditionalData = $origItem->getAdditionalData();
            $originAdditionalData = array_merge($originAdditionalData, $bodyParams['additional_data']);
            $origItem->setAdditionalData($originAdditionalData);
        }
        return $origItem;
    }

    public function setVoucherCartTotals($quote, $cartTotals)
    {
        $giftCards = [];
        $extentionAttributes = $quote->getExtensionAttributes();
        if ($extentionAttributes) {
            $shippingAssignments = $extentionAttributes->getShippingAssignments();
            foreach ($shippingAssignments as $shippingAssignment) {
                $shipping = $shippingAssignment->getShipping();
                if ($shipping) {
                    $totalsCollector = $this->objectManager->create('Magento\Quote\Model\Quote\TotalsCollector');
                    $giftCardAccountHelper = $this->objectManager->create('Viapresse\GiftCardAccount\Model\Total\Quote\Giftcardaccount');
                    $address = $shipping->getAddress();
                    $addressTotals = $totalsCollector->collectAddressTotals($quote, $address);
                    $addressGiftCard = $giftCardAccountHelper->fetch($quote, $addressTotals);
                    if ($addressGiftCard) {
                        $giftCards[] = $addressGiftCard;
                    }
                }
            }

            if (count($giftCards) > 0) {
                $giftCard = $giftCards[0];
                if ($cartTotals->getTotalSegments()) {
                    if (isset($cartTotals->getTotalSegments()['giftcardaccount'])) {
                        $giftCardAccount = $cartTotals->getTotalSegments()['giftcardaccount'];
                        $giftCardAccount->setValue($giftCard['value']);
                        $giftCardExtensions = $giftCardAccount->getExtensionAttributes();
                        $giftCardExtensions->setGiftCards(json_encode($giftCard['gift_cards']));
                    }
                }
            }
        }
    }
}
