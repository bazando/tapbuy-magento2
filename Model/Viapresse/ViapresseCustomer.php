<?php

namespace Tapbuy\Checkout\Model\Viapresse;

class ViapresseCustomer
{

    /**
     * Retrieve customer default shipping address
     *
     * @param \Viapresse\Customer\Model\Data\Customer $customer
     * @return int|null
    */
    public function getCustomerDefaultShippingAddress($customer)
    {
        $defalutAddressId = $customer->getDefaultShipping();

        if (!$defalutAddressId && count($customer->getAddresses()) > 0) {
            $defalutAddressId = $customer->getAddresses()[0]->getId();
        }
        return  $defalutAddressId;
    }

    /**
     * Retrieve customer default billing address
     *
     * @param \Viapresse\Customer\Model\Data\Customer $customer
     * @return int|null
    */
    public function getCustomerDefaultBillingAddress($customer)
    {
        $defalutAddressId = $customer->getDefaultBilling();

        if (!$defalutAddressId && count($customer->getAddresses()) > 0) {
            $defalutAddressId = $customer->getAddresses()[0]->getId();
        }
        return  $defalutAddressId;
    }

}
