<?php

namespace Tapbuy\Checkout\Model\Viapresse;
use Tapbuy\Checkout\Api\Viapresse\GiftOptionInterface;
use Tapbuy\Checkout\Model\Cart;

class ViapresseGiftOptions implements GiftOptionInterface
{
    protected $objectManager;
    protected $options;
    protected $productHelperConfigFactory;
    protected $productRepository;
    protected $escaper;
    protected $scopeConfig;
    /**
     *
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;

    /**
     * Tapbuy Cart
     * @var Cart
     */
    protected $tapbuyCart;

    /**
     *
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(\Magento\Framework\Webapi\Rest\Request $request, Cart $tapbuyCart)
    {
        $this->_request = $request;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->options = $this->objectManager->create('Viapresse\GiftOptions\Model\Source\GiftOptions');
        $this->productHelperConfigFactory = $this->objectManager->create('Viapresse\GiftProduct\Helper\Config');
        $this->productRepository = $this->objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
        $this->escaper = $this->objectManager->create('Magento\Framework\Escaper');
        $this->scopeConfig = $this->objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $this->tapbuyCart = $tapbuyCart;
    }

    private function getFullNameRecipient($item)
    {
        if (!isset($item->getAdditionalData()['shipping_address'])) {
            return null;
        }
        $address = $item->getAdditionalData()['shipping_address'];

        $firstName = $address['firstname']??'';
        $lastName = $address['lastname']??'';

        return sprintf('%s %s', ucfirst($firstName), mb_strtoupper($lastName));
    }

    private function getActionSentence($item)
    {
        $str = __('Customize the option for')->render();
        $str .= ' ';
        $str .= $this->getFullNameRecipient($item);
        $str .= ' : ';
        if ($item->getProduct()->getSku() === $this->options::VOUCHER) {
            $str .= __('Type your message')->render();
        } else {
            $str .= __('Choose a card template, type your message and indicate the date of reception')->render();
        }

        return $str;
    }

    private function getSecondaryActionSentence($item)
    {
        $str = null;
        if ($item->getParent() && $item->getParent()->getProduct()->getSku() === $this->productHelperConfigFactory->getSkuGiftCardProduct()) {
            $str = $this->escaper->escapeHtml(
                __('You will find your e-card voucher in your customer account, my orders tab after making your payment')->render()
            );
        } else {
            $str = $this->escaper->escapeHtml(
                __('You will find your print voucher in your customer account, my orders tab after making your payment')->render()
            );
        }
        return $str;
    }

    private function getMaxLengthMessage()
    {
        return $this->scopeConfig->getValue(
            'via_gift_options/settings_form/max_length_message',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    private function getLabel($item)
    {
        switch ($item->getProduct()->getSku()) {
            case $this->options::READY_TO_OFFER:
                $label = __($this->options::READY_TO_OFFER_LABEL)->render();
                break;
            case $this->options::VOUCHER:
                $label = __($this->options::VOUCHER_LABEL)->render();
                break;
            case $this->options::GREETING_CARD:
                $label = __($this->options::GREETING_CARD_LABEL)->render();
                break;
            default:
                $label = null;
                break;
        }
        return $label;
    }

    private function getType($item)
    {
        switch ($item->getProduct()->getSku()) {
            case $this->options::READY_TO_OFFER:
                $type = 'ready_to_offer';
                break;
            case $this->options::VOUCHER:
                $type = 'voucher';
                break;
            case $this->options::GREETING_CARD:
                $type = 'greeting_card';
                break;
            default:
                $type = null;
                break;
        }
        return $type;
    }

    private function getCardModelList($item)
    {
        $productId = $item->getProduct()->getId();
        $list = [];
        $product = $this->productRepository->getById($productId);
        $mediaGalleryImagesCollection = $product->getMediaGalleryImages();
        $mediaGalleryImagesCollection->setOrder('position');

        // Shift an element off the beginning of array which corresponds to the gift card model.
        $mediaGalleryImages = $mediaGalleryImagesCollection->toArray()['items'];
        array_shift($mediaGalleryImages);

        if (is_array($mediaGalleryImages)) {
            foreach ($mediaGalleryImages as $image) {
                $list[] = [
                    'value' => $image['file'],
                    'imageUrl' => $image['url'],
                    'label' => null,
                ];
            }
        }
        return $list;
    }

    private function getItemsToRender($item)
    {
        $sku = $item->getProduct()->getSku();
        $parentSku = $item->getParentItem()->getProduct()->getSku();

        // Rendering for only 'ready to offer' & 'greeting card' gift options.
        if (in_array(
            $sku,
            [$this->options::READY_TO_OFFER, $this->options::GREETING_CARD]
        )) {
            $itemsToRender = [
                [
                    'name' => 'card_model',
                    'type' => 'radio',
                    'label' => null,
                    'items' => $this->getCardModelList($item),
                ],
                [
                    'name' => 'delivery',
                    'type' => 'radio',
                    'label' => __('Indicate the date of receipt for your')->render() . ' ' . $this->getLabel($item),
                    'items' => [
                        [
                            'label' => __('As soon as possible (2 to 4 working days)')->render(),
                            'value' => 1,
                        ],
                        [
                            'label' => __('The day of my choice (excluding weekends and public holidays)')->render(),
                            'value' => 2,
                        ],
                    ],
                ],
                [
                    'name' => 'date',
                    'type' => 'datetime',
                    'label' => null,
                    'conditional' => [
                        'operator' => 'and',
                        'conditions' => [
                            [
                                'fieldName' => 'delivery',
                                'operator' => 'equals',
                                'value' => 2,
                            ],
                        ],
                    ],
                ],
            ];
        }

        if ($sku === $this->options::VOUCHER &&
            $parentSku === $this->productHelperConfigFactory->getSkuGiftCardProduct()) {
            $itemsToRender = [
                [
                    'name' => 'prefix',
                    'type' => 'radio',
                    'label' => __('Civility'),
                    'items' => [
                        [
                            'label' => __('Mister'),
                            'value' => 'mister',
                            'checked' => true,
                        ],
                        [
                            'label' => __('Madame'),
                            'value' => 'madame',
                        ],
                    ],
                ],
                [
                    'name' => 'firstname',
                    'type' => 'text',
                    'label' => __('Firstname'),
                ],
                [
                    'name' => 'lastname',
                    'type' => 'text',
                    'label' => __('Lastname'),
                ],
            ];
        }
        // Rendering for all types of gift options.
        $itemsToRender[] = [
            'name' => 'message',
            'type' => 'textarea',
            'maxlength' => $this->getMaxLengthMessage(),
            'label' => __('Your message')->render(),
            'placeholder' => $this->escaper->escapeHtmlAttr(__(
                'Your message (in %1 characters)',
                $this->getMaxLengthMessage()
            )->render()),
        ];

        return $itemsToRender;
    }

    /**
     * Check if cart has gift options
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool
     */
    public function hasGiftOptions($quote)
    {
        $giftSKUs = [$this->options::READY_TO_OFFER, $this->options::VOUCHER, $this->options::GREETING_CARD];
        foreach ($quote->getAllItems() as $item) {
            foreach ($item->getChildren() as $child) {
                $sku = $child->getProduct()->getSku();
                if (in_array($sku, $giftSKUs)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get cart items and  gift options
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function getGiftOptions($quote)
    {
        $giftSKUs = [$this->options::READY_TO_OFFER, $this->options::VOUCHER, $this->options::GREETING_CARD];
        $quoteItems = [];
        foreach ($quote->getAllItems() as $item) {
            foreach ($item->getChildren() as $child) {
                $sku = $child->getSku();
                if (in_array($sku, $giftSKUs)) {
                    $additionalData = $child->getAdditionalData();
                    $giftOption = [];

                    $giftOption['card_model_list'] = $this->getCardModelList($child);
                    $giftOption['type'] = $this->getType($child);
                    $giftOption['parent_item_id'] = $child->getParentItemId();
        
                    // $giftOption['itemsToRender'] = $this->getItemsToRender($child);    
                    // $giftOption['actionSentence'] = $this->getActionSentence($child);

                    // if ($sku === $this->options::VOUCHER) {
                    //     $giftOption['secondaryActionSentence'] = $this->getSecondaryActionSentence($child);
                    // }

                    $additionalData = array_merge($additionalData, [
                        'tapbuy_gift_option' => $giftOption,
                    ]);
                    $child->setAdditionalData($additionalData);
                    $quoteItems[] = $child;
                }
            }
        }
        return $quoteItems;
    }

    /**
     * Set products gift options
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
    */
    public function setItemsGiftOptions($item)
    {
        $giftSKUs = [$this->options::READY_TO_OFFER, $this->options::VOUCHER, $this->options::GREETING_CARD];
        
        foreach ($item->getChildren() as $child) {
            $sku = $child->getProduct()->getSku();
            if (in_array($sku, $giftSKUs)) {
                $additionalData = $item->getAdditionalData();
                $giftOption = [];
    
                $giftOption['itemsToRender'] = $this->getItemsToRender($child);    
                $giftOption['actionSentence'] = $this->getActionSentence($child);

                if ($sku === $this->options::VOUCHER) {
                    $giftOption['secondaryActionSentence'] = $this->getSecondaryActionSentence($child);
                }

                $additionalData = array_merge($additionalData, [
                    'tapbuy_gift_option' => $giftOption,
                ]);
                $item->setAdditionalData($additionalData);
            }
        }
    }

    public function update($quoteId, $itemId)
    {
        $bodyParams = $this->_request->getBodyParams();
        $giftOptionsQuoteItemAdditionalDataObject = $this->objectManager->create('Viapresse\GiftOptions\Model\AdditionalDataObject\QuoteItemAdditionalDataObject');
        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');

        // get cart
        $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
        $quote = $quoteInterceptor->get($quoteId);
        
        $giftOptionId = $bodyParams['gift_option_id'] ?? null;
        $parent = $quote->getItemById($itemId);
        // $item = $parent->getItemById($giftOptionId);
        $item = null;
        foreach ($parent->getChildren() as $child) {
            if ($child->getItemId() == $giftOptionId) {
                $item = $child;
                break;
            }
        }
        
        if (!$parent || !$item) {
            return $this->tapbuyCart->getCart($quoteId);
        }
        $quoteItemAdditionalData = $item->getAdditionalData();
        if (isset($quoteItemAdditionalData[$giftOptionsQuoteItemAdditionalDataObject::ROOT])) {
            $giftOptionsQuoteItemAdditionalDataObject->setAllData(
                $quoteItemAdditionalData[$giftOptionsQuoteItemAdditionalDataObject::ROOT]
            );
        } else {
            $giftOptionsQuoteItemAdditionalDataObject->initData();
        }

        $giftOptionsQuoteItemAdditionalDataObject
            ->setMessage($bodyParams['message'] ?? '')
            ->setFirstname($bodyParams['firstname'] ?? '')
            ->setLastname($bodyParams['lastname'] ?? '')
            ->setPrefix($bodyParams['prefix'] ?? '')
            ->setDeliveryTime($bodyParams['delivery'] ?? '1')
            ->setCardModel($bodyParams['card_model'] ?? 'null')
            ->setDeliveryDate($bodyParams['delivery_data'] ?? '');

        $additionalData = array_merge(
            $quoteItemAdditionalData,
            $giftOptionsQuoteItemAdditionalDataObject->toArray()
        );
        $item->setAdditionalData($additionalData);
        $quoteRepository->save($quote);

        return $this->tapbuyCart->getCart($quoteId);
    }
}
