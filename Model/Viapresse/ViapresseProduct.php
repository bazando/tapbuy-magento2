<?php

namespace Tapbuy\Checkout\Model\Viapresse;

class ViapresseProduct
{
    public function setProductCoverImage($product, $tapbuyProduct)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $coverImageUrlParam = '';
        if ($product->getVpTitleId()) {
            $issueManagement = $objectManager->create('Viapresse\Catalog\Model\IssueManagement');
            $issue = $issueManagement->getLastIssueByTitleSku($product->getVpTitleId());

            if ($issue) {
                $coverImageUrlParam = $issue->getNumber();
            }

        }

        $viaCatalogHelperData = $objectManager->create('Viapresse\Catalog\Helper\Data');
        $coverImageUrl = $viaCatalogHelperData->getCoverImageUrl($product, 800);
        $tapbuyProduct->setCoverImageUrl($coverImageUrl . '?' . $coverImageUrlParam);

        return $tapbuyProduct;
    }
}
