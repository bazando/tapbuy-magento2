<?php

namespace Tapbuy\Checkout\Model\Viapresse;
use Magento\Framework\App\Area;
use Magento\Store\Model\ScopeInterface;

class ViapresseOrder
{
    private $objectManager;

    public function __construct()
    {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    private function hasGiftCardInOrder($order)
    {
        $viaGiftHelper = $this->objectManager->create('Viapresse\GiftProduct\ViewModel\GiftProduct');
        foreach ($order->getAllItems() as $orderItem) {
            if ($viaGiftHelper->isGiftcardProduct($orderItem->getSku())) {
                return true;
            }
        }
        return false;
    }

    public function getCustomMessage($paymentMethod, $order, $customMessage) {
        $tapbuyHelper = $this->objectManager->create('Tapbuy\Checkout\Helper\Data');
        $isCheckmo = $paymentMethod->getMethod() === 'checkmo';
        if ($isCheckmo) {
            $appEmulation = $this->objectManager->create('Magento\Store\Model\App\Emulation');
            $appEmulation->startEnvironmentEmulation($order->getStoreId(), Area::AREA_FRONTEND, true);
            $checkoutHelper = $this->objectManager->create('Magento\Checkout\Helper\Data');
            $escaper = $this->objectManager->create('Magento\Framework\Escaper');
            $storeName = $tapbuyHelper->getConfig('general/store_information/name', ScopeInterface::SCOPE_STORE);

            $customMessage .= "<h2 class=\"d-none d-print-block\" style=\"line-height: normal\">" . $escaper->escapeHtml(__('Order number: %1', $order->getIncrementId())) . "</h2><p><span class=\"d-print-none\">" . __('It will be processed by our teams upon receipt of your payment.') . "</span></p>" .
            "<p>" . __("Print and sign this form and send it by mail with your payment to to this address") . "<br>" .
            "<b>" . $escaper->escapeHtml($tapbuyHelper->getConfig('payment/checkmo/mailing_address')) . "</b></p>" .
            "<p>" . __(
                'Join your check of %1 payable to %2',
                '<b>' . $checkoutHelper->formatPrice($order->getGrandTotal()) . '</b>',
                '<b>' . $escaper->escapeHtml($storeName . ' / Vialife') . '</b>'
            ) . "</p><p>" . __('We accept the following gift vouchers: Cadhoc, UP Chèque Lire, Kadéos, Tir Groupé, Cado Bimpli, Shopping Pass.') . "</p>" .
            "<p>" . __(
                'If your gift voucher is worth less than the total amount of your order, you can make the supplement by bank check payable to %1.',
                $escaper->escapeHtml($storeName)
            ) . "<br>" . __(
                'If your gift voucher is greater than the total amount of your order, %1 does not refund the difference.',
                $escaper->escapeHtml($storeName)
            ) . "</p><div class=\"d-none d-print-block\"><h4>" . $escaper->escapeHtml(__('Your signature (mandatory)')) . "</h4>
            <textarea rows=\"6\" cols=\"0\" style=\"width:100%\"></textarea></div>" .
            "<button class=\"btn btn-default\" onclick=\"(function print(){window.print();return false;})();return false;\">" .
            $escaper->escapeHtml(__('Print')) . "</button>";
        }

        if ($this->hasGiftCardInOrder($order)) {
            // is payment method as check mode ?
            if ($isCheckmo) {
                $customMessage .=  '<p>' . __('You will find your voucher to be printed in your customer account as soon as we receive your payment')->render() . '</p>';
            } else {
                $customMessage .=  '<p>' . __('You will find your print voucher in your customer account')->render() . '</p>';
            }
        }

        return $customMessage;
    }
}
