<?php

namespace Tapbuy\Checkout\Model\Order;

class Payment
{
    private \Magento\Paypal\Model\ConfigFactory $paypalConfigFactory;
    private \Magento\Paypal\Model\CartFactory $cartFactory;
    private \Magento\Paypal\Model\Api\Type\Factory $paypalApiFactory;
    private \Magento\Sales\Model\OrderRepository $orderRepository;
    private \Magento\Framework\Webapi\Request $request;

    /**
     * constructor
     *
     * @param \Magento\Paypal\Model\ConfigFactory $paypalConfigFactory
     * @param \Magento\Paypal\Model\CartFactory $cartFactory
     * @param \Magento\Paypal\Model\Api\Type\Factory $paypalApiFactory
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     */
    public function __construct(
        \Magento\Paypal\Model\ConfigFactory $paypalConfigFactory,
        \Magento\Paypal\Model\CartFactory $cartFactory,
        \Magento\Paypal\Model\Api\Type\Factory $paypalApiFactory,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Webapi\Request $request
    ) {
        $this->paypalConfigFactory = $paypalConfigFactory;
        $this->cartFactory = $cartFactory;
        $this->paypalApiFactory = $paypalApiFactory;
        $this->orderRepository = $orderRepository;
        $this->request = $request;
    }

    /**
     *
     * @param \Magento\Sales\Model\Order\Payment $subject
     * @param mixed $result
     */
    public function afterPlace(\Magento\Sales\Model\Order\Payment $subject, $result)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        // check Tapbuy is enabled
        $tapbuyHelper = $objectManager->create('\Tapbuy\Checkout\Helper\Data');
        $isEnabled = $tapbuyHelper->getConfig('tapbuy_checkout/general/enabled');
        $isEnabled = true;
        if ($isEnabled) {
            $request = $objectManager->create('Magento\Framework\Webapi\Request');

            // check if the Tapbuy header is present
            // we have to cnnage the redirect URLs only in Tapbuy context
            if ($request->getHeader('X-Tapbuy-Call')) {
                $order = $subject->getOrder();
                $payment = $order->getPayment();

                if ($payment->getMethod() === 'paypal_express') {
                    $configParameters = ['params' => [
                            \Magento\Paypal\Model\Config::METHOD_WPP_EXPRESS,
                            $order->getStoreId()
                        ]
                    ];

                    /** @var \Magento\Paypal\Model\Config $paypalConfig */
                    $paypalConfig = $this->paypalConfigFactory->create($configParameters);
                    $paypalConfig->setMethodInstance($payment->getMethodInstance());

                    /** @var \Magento\Paypal\Model\Cart $cart */
                    $cart = $this->cartFactory->create(['salesModel' => $order]);

                    /** @var \Magento\Paypal\Model\Api\Nvp $api */
                    $api = $this->paypalApiFactory->create(\Magento\Paypal\Model\Api\Nvp::class)
                        ->setConfigObject($paypalConfig);

                    $solutionType = $paypalConfig->getMerchantCountry() == 'DE'
                            ? \Magento\Paypal\Model\Config::EC_SOLUTION_TYPE_MARK
                            : $paypalConfig->getValue('solutionType');

                    $api->setAmount(round((float)$order->getBaseGrandTotal(), 2))
                        ->setCurrencyCode($order->getBaseCurrencyCode())
                        ->setInvNum($order->getIncrementId())
                        ->setSolutionType($solutionType)
                        ->setPaymentAction($paypalConfig->getValue('paymentAction'))
                        ->setBillingAddress($order->getBillingAddress())
                        ->setAddress($order->getBillingAddress())
                        ->setSuppressShipping(true)
                        ->setPaypalCart($cart)
                        ->setIsLineItemsEnabled($paypalConfig->getValue('lineItemsEnabled'));

                    $requestBody = json_decode($request->getContent());

                    if (is_object($requestBody)) {
                        if (isset($requestBody->paymentMethod->additional_data->accept_url)) {
                            $api->setReturnUrl($requestBody->paymentMethod->additional_data->accept_url);
                        }
                        if (isset($requestBody->paymentMethod->additional_data->cancel_url)) {
                            $api->setCancelUrl($requestBody->paymentMethod->additional_data->cancel_url);
                        }
                        if (isset($requestBody->paymentMethod->additional_data->notify_url)) {
                            $api->setNotifyUrl($requestBody->paymentMethod->additional_data->notify_url);
                        }
                    }

                    if ($paypalConfig->getValue('requireBillingAddress') == \Magento\Paypal\Model\Config::REQUIRE_BILLING_ADDRESS_ALL) {
                        $api->setRequireBillingAddress(1);
                    }

                    $api->setBillingType($api->getBillingAgreementType());
                    $paypalConfig->exportExpressCheckoutStyleSettings($api);

                    $api->callSetExpressCheckout();

                    $token = $api->getToken();
                    $redirectUrl = $paypalConfig->getPayPalBasicStartUrl($token);

                    $payment->setAdditionalInformation(\Magento\Paypal\Model\Express\Checkout::PAYMENT_INFO_TRANSPORT_TOKEN, $token)
                        ->setAdditionalInformation(\Magento\Paypal\Model\Express\Checkout::PAYMENT_INFO_TRANSPORT_REDIRECT, $redirectUrl)
                        ->setAdditionalInformation('redirectUrl', $redirectUrl)
                        ->setAdditionalInformation(\Magento\Paypal\Model\Express\Checkout::PAYMENT_INFO_TRANSPORT_PAYER_ID, $api->getPayerId());

                    $order->setPayment($payment);
                    $subject->setOrder($order);
                }
            }
        }
        return $result;
    }
}
