<?php
namespace Tapbuy\Checkout\Model\Sentry;

use Magento\Framework\App\Request\Http as HttpRequest;
use Magento\Framework\Webapi\Rest\Response\Proxy as HttpResponse;
use Sentry\SentrySdk;
use Sentry\Tracing\Transaction;
use Sentry\Tracing\TransactionContext;
use Sentry\Tracing\TransactionSource;
use Sentry\Tracing\SpanContext;

class SentryPerformance
{
    /** @var Transaction|null */
    private $transaction;

    public function startTransaction(HttpRequest $request)
    {

        $requestStartTime = $request->getServer('REQUEST_TIME_FLOAT', microtime(true));

        $context = TransactionContext::fromHeaders(
            $request->getHeader('sentry-trace') ?: '',
            $request->getHeader('baggage') ?: ''
        );

        $requestPath = '/' . ltrim($request->getRequestUri(), '/');

        $context->setOp('http.server');
        $context->setName($requestPath);
        $context->setSource(TransactionSource::url());
        $context->setStartTimestamp($requestStartTime);

        $context->setData([
            'url'    => $requestPath,
            'method' => strtoupper($request->getMethod()),
        ]);

        // Start the transaction
        $transaction = \Sentry\startTransaction($context);

        // If this transaction is not sampled, don't set it either and stop doing work from this point on
        if (!$transaction->getSampled()) {
            return;
        }

        // Set the current transaction as the current span so we can retrieve it later
        SentrySdk::getCurrentHub()->setSpan($transaction);

        $this->transaction = $transaction;
    }

    public function finishTransaction($response)
    {
        if ($this->transaction) {
            if ($response instanceof HttpResponse) {
                $this->transaction->setHttpStatus($response->getStatusCode());
            }

            // Finish the transaction, this submits the transaction and it's span to Sentry
            $this->transaction->finish();

            $this->transaction = null;
        }
    }

    public function startSpanContext($op, $description, $startTime = null) {
        $parentSpan = SentrySdk::getCurrentHub()->getSpan();
        if (!$parentSpan) {
            return;
        }

        $context = new SpanContext();
        $context->setOp($op);
        $context->setDescription($description);
        $context->setStartTimestamp($startTime ?: microtime(true));

        return $context;
    }

    public function endSpanContext($context, $endTime = null) {
        $parentSpan = SentrySdk::getCurrentHub()->getSpan();
        if (!$parentSpan) {
            return;
        }

        $context->setEndTimestamp($endTime ?: microtime(true));

        $parentSpan->startChild($context);
    }

    public function startSqlQuery($sql, $startTime = null)
    {
        $parentSpan = SentrySdk::getCurrentHub()->getSpan();
        if (!$parentSpan) {
            return;
        }
        $context = $this->startSpanContext('db.sql.query', $sql, $startTime);

        return $context;
    }

    public function endSqlQuery($context, $endTime = null)
    {
        $this->endSpanContext($context, $endTime);
    }
}
