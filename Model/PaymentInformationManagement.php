<?php

namespace Tapbuy\Checkout\Model;

use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\PaymentInterface;

class PaymentInformationManagement implements \Tapbuy\Checkout\Api\PaymentInformationManagementInterface
{
    /**
     *
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;
    private $objectManager;
    private $retailerName;

    /**
     *
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(\Magento\Framework\Webapi\Rest\Request $request)
    {
        $this->_request = $request;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->retailerName = $this->getRetailerName();
    }

    private function getRetailerName()
    {
        $tapbuyHelper = $this->objectManager->create('Tapbuy\Checkout\Helper\Data');
        return $tapbuyHelper->getConfig('tapbuy_checkout/general/retailer_name');
    }

    private function setOrderCustomMessage($paymentMethod, $order): void
    {

        $customMessage = '';

        if ($this->retailerName == 'viapresse') {
            $viapresseOrder = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseOrder');
            $customMessage = $viapresseOrder->getCustomMessage($paymentMethod, $order, $customMessage);
        }

        if (!empty($customMessage)) {
            $extentionAttributes = $order->getExtensionAttributes();
            $extentionAttributes->setTapbuyCustomMessage($customMessage);
            $order->setExtensionAttributes($extentionAttributes);
        }

    }

    /**
     * {@inheritDoc}
     */
    public function savePaymentInformationAndPlaceOrder($cartId, PaymentInterface $paymentMethod, AddressInterface $billingAddress = null)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();
        $requestParams = $this->_request->getParams();
        $requestParams['cartId'] = $cartId;
        $this->_request->setParams($requestParams);

        if ($bodyParams['is_guest']) {
            // it's a guest cart we want to pay, get the masked id of the cart
            $quoteIdToMaskedQuoteId = $this->objectManager->create('Magento\Quote\Model\QuoteIdToMaskedQuoteId');
            $maskedId = $quoteIdToMaskedQuoteId->execute($cartId);

            // set payment info and place order
            $guestPaymentInterceptor = $this->objectManager->create('Magento\Checkout\Model\GuestPaymentInformationManagement\Interceptor');
            $orderId = $guestPaymentInterceptor->savePaymentInformationAndPlaceOrder($maskedId, $bodyParams['email'], $paymentMethod, $billingAddress);

        } else {
            // it's the cart of a logged user, set payment info and place order
            $paymentInterceptor = $this->objectManager->create('Magento\Checkout\Model\PaymentInformationManagement\Interceptor');
            $orderId = $paymentInterceptor->savePaymentInformationAndPlaceOrder($cartId, $paymentMethod, $billingAddress);
        }

        $orderRepository = $this->objectManager->create('Magento\Sales\Model\OrderRepository');
        $order = $orderRepository->get($orderId);

        // specific code for some retailer
        if (!empty($bodyParams['retailer_specific'])) {
            if (!empty($bodyParams['retailer_specific']['retailer_id'])) {
                $order->setData('retailer_id', $bodyParams['retailer_specific']['retailer_id']);
                $order->save();
            }
        }

        $this->setOrderCustomMessage($paymentMethod, $order);

        return $order;
    }

    /**
     * {@inheritDoc}
     */
    public function savePaymentInformation($cartId, PaymentInterface $paymentMethod, AddressInterface $billingAddress = null)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        if ($bodyParams['is_guest']) {
            // it's a guest cart we want to pay, get the masked id of the cart
            $quoteIdToMaskedQuoteId = $this->objectManager->create('Magento\Quote\Model\QuoteIdToMaskedQuoteId');
            $maskedId = $quoteIdToMaskedQuoteId->execute($cartId);

            // set payment info and place order
            $guestPaymentInterceptor = $this->objectManager->create('Magento\Checkout\Model\GuestPaymentInformationManagement\Interceptor');
            $orderId = $guestPaymentInterceptor->savePaymentInformation($maskedId, $bodyParams['email'], $paymentMethod, $billingAddress);

        } else {
            // it's the cart of a logged user, set payment info and place order
            $paymentInterceptor = $this->objectManager->create('Magento\Checkout\Model\PaymentInformationManagement\Interceptor');
            $orderId = $paymentInterceptor->savePaymentInformation($cartId, $paymentMethod, $billingAddress);
        }

        // specific to Payapl when used directly (no PSP) we need to generate the Payapl redirect URL
        if($bodyParams['paymentMethod']['method'] == 'paypal_express') {
            $paypalExpress = $this->objectManager->create('Tapbuy\Checkout\Model\Paypal\Express');
            $paypalExpress->generateRedirectUrl($orderId, $bodyParams['paymentMethod']['additional_data']['accept_url'], $bodyParams['paymentMethod']['additional_data']['cancel_url'], null, true);
        }

        $orderRepository = $this->objectManager->create('Magento\Sales\Model\OrderRepository');
        $order = $orderRepository->get($orderId);

        // specific code for some retailer
        if (!empty($bodyParams['retailer_specific'])) {
            if (!empty($bodyParams['retailer_specific']['retailer_id'])) {
                $order->setData('retailer_id', $bodyParams['retailer_specific']['retailer_id']);
                $order->save();
            }
        }

        return $order;
    }
}
