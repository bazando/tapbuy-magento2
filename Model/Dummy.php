<?php

namespace Tapbuy\Checkout\Model;

use Tapbuy\Checkout\Api\DummyInterface as DummyInterface;

class Dummy implements DummyInterface
{

    /**
     * Dummy function to test the API
     *
     * @api
     * @param string $dummy
     * @return string
     */
    public function DummyData($dummy)
    {
        return 'Dummy param: ' . $dummy;
    }
}
