<?php

namespace Tapbuy\Checkout\Model;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Quote\Api\Data\AddressInterface;
use Tapbuy\Checkout\Api\CartInterface;

class Cart implements CartInterface
{
    /**
     *
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;
    private $objectManager;
    private $retailerName;
    private $timingEvents;

    /**
     *
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(\Magento\Framework\Webapi\Rest\Request $request)
    {
        $this->_request = $request;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->retailerName = $this->getRetailerName();
        $this->timingEvents = new \Tapbuy\Checkout\Helper\TimingEvents();
    }

    private function getRetailerName()
    {
        $tapbuyHelper = $this->objectManager->create('Tapbuy\Checkout\Helper\Data');
        return $tapbuyHelper->getConfig('tapbuy_checkout/general/retailer_name');
    }

    private function setShippingAmount($quoteId)
    {
        $quoteReopository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');
        // ! Bottleneck here (6.93%), we should try to pass the quote object instead of the quoteId
        $quote = $quoteReopository->get($quoteId);
        $shippingAmount = 0.0;

        $shippingAddresses = $quote->getAllShippingAddresses();
        foreach ($shippingAddresses as $shippingAddress) {
            $allShippingRates = $shippingAddress->getAllShippingRates();
            
            foreach ($allShippingRates as $shippingRate) {
                $shippingAmount += $shippingRate->getPrice();
            }
        }
        $quote->setShippingAmount($shippingAmount);
        $quote->setTriggerRecollect(0);
        $quote->setTotalsCollectedFlag(true);

        // Save fails for non logged in customers
        try {
            // ! Bottleneck here (1.67%), we should try without saving the quote here
            $quoteReopository->save($quote);
        } catch (\Exception $e) {
            // Do nothing
        }
        return $quote;
    }

    /**
     * get quoteId from maskedId
     * @param string $maskedId
     * @return int
     */
    protected function getCartIdFromMaskedId($maskedId)
    {
        $quoteId = $maskedId;
        if (!is_numeric($maskedId)) {
            $quoteIdMask = $this->objectManager->create('Magento\Quote\Model\QuoteIdMask');
            $quoteIdMask->load($maskedId, 'masked_id');
            $quoteId = $quoteIdMask->getQuoteId();
        }

        return $quoteId;
    }

    /**
     * generic funcion to get a cart
     * @param string $quoteId
     * @return \Magento\Quote\Api\Data\CartInterface
     */
    protected function getCartGeneric($quoteId)
    {

        // check if it's masked quote ID on a normal quote id
        if (!is_numeric($quoteId)) {
            $interceptor = $this->objectManager->create('Magento\Quote\Model\GuestCart\GuestCartRepository');
        } else {
            $interceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
        }

        return $interceptor->get($quoteId);
    }

    /**
     * @param string $quoteId
     * @return \Magento\Quote\Api\Data\TotalsInterface
     */
    public function getCartTotals($quoteId)
    {
        // get the cart
        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');
        $quote = $quoteRepository->get($quoteId);
        $totalsInterceptor = $this->objectManager->create('Magento\Quote\Model\Cart\CartTotalRepository\Interceptor');
        try {
            $cartTotals = $totalsInterceptor->get($quoteId);
        } catch (\Throwable $e) {
            $cartTotals = $this->collectCartTotals($quote);
        }
        if ($this->retailerName == 'viapresse') {
            $viapresseCart = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseCart');
            $viapresseCart->setVoucherCartTotals($quote, $cartTotals);
        }

        return $cartTotals;
    }
    
    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::getCart()
     */
    public function getCart($quoteId)
    {
        // check if it's masked quote ID on a normal quote id
        if (!is_numeric($quoteId)) {
            // get the cart
            $interceptor = $this->objectManager->create('Magento\Quote\Model\GuestCart\GuestCartRepository');
            $quote = $interceptor->get($quoteId);

            // get the cart totals
            $totalsInterceptor = $this->objectManager->create('Magento\Quote\Model\Cart\CartTotalRepository\Interceptor');
            $cartTotals = $totalsInterceptor->get($quote->getId());

            $tapbuyCart = new \Tapbuy\Checkout\Api\Data\TapbuyCart();
            $tapbuyCart->setCart($quote);
            $tapbuyCart->setCartTotals($cartTotals);
            $tapbuyCart->setMaskedId($quoteId);

            return $tapbuyCart;
        }
        // get the cart
        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');
        $quoteFactory = $this->objectManager->create('Magento\Quote\Model\QuoteFactory');

        $quote = $quoteFactory->create();
        $quoteResource = $quote->getResource();

        $quoteResource->getConnection()->beginTransaction();

        $quote = $quoteRepository->get($quoteId);

        $quote->setIsMultiShipping(true);

        $quoteResource->getConnection()->commit();

        // get the flag indicating if we want the masked_id of the cart
        $withMaskedId = $this->_request->getParam('with_masked_id', false);
        if (!$withMaskedId) {
            $withMaskedId = $this->_request->getHeader('X-Tapbuy-Masked-Id', false);
        }

        $maskedId = null;
        if ($withMaskedId) {
            if ($quote->getCustomerIsGuest()) {
                $maskedId = $this->getCartMaskedId($quoteId);
            }
        }

        if ($this->retailerName == 'viapresse') {
            $viapresseCart = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseCart');
            $viapresseCart->getCartBeforeTotals($this->_request, $quoteId, $quote);
        } else {
            $viapresseCart = null;
        }
        // ! Bottleneck here (8.61%), find a way to optimize this
        $this->setShippingAmount($quoteId);

        // get the cart totals
        // ! Bottleneck here (9.02%), find a way to optimize this
        $cartTotals = $this->getCartTotals($quoteId);

        $tapbuyCart = new \Tapbuy\Checkout\Api\Data\TapbuyCart();
        $tapbuyCart->setCart($quote);
        $tapbuyCart->setCartTotals($cartTotals);
        if ($this->retailerName == 'viapresse') {
            $tapbuyCart = $viapresseCart->getCartAfterTotals($quote, $tapbuyCart);
        }
        $tapbuyCart->setMaskedId($maskedId);

        return $tapbuyCart;
    }

    private function collectCartTotals($quote)
    {
        $totalsFactory = $this->objectManager->create('Magento\Quote\Api\Data\TotalsInterfaceFactory');
        $dataObjectHelper = $this->objectManager->create('Magento\Framework\Api\DataObjectHelper');
        $itemConverter = $this->objectManager->create('Magento\Quote\Model\Cart\Totals\ItemConverter');
        $totalsConverter = $this->objectManager->create('Magento\Quote\Model\Cart\TotalsConverter');
        $couponService = $this->objectManager->create('Magento\Quote\Api\CouponManagementInterface');
        $addressTotalsData = $quote->getShippingAddress()->getData();
        $addressTotals = $quote->getShippingAddress()->getTotals();
        unset($addressTotalsData[\Magento\Framework\Api\ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY]);

        $quoteTotals = $totalsFactory->create();
        $dataObjectHelper->populateWithArray(
            $quoteTotals,
            $addressTotalsData,
            \Magento\Quote\Api\Data\TotalsInterface::class
        );
        $items = array_map([$itemConverter, 'modelToDataObject'], $quote->getAllVisibleItems());
        $calculatedTotals = $totalsConverter->process($addressTotals);
        $quoteTotals->setTotalSegments($calculatedTotals);

        $amount = $quoteTotals->getGrandTotal() - $quoteTotals->getTaxAmount();
        $amount = $amount > 0 ? $amount : 0;
        $quoteTotals->setCouponCode($couponService->get($quote->getId()));
        $quoteTotals->setGrandTotal($amount);
        $quoteTotals->setItems($items);
        $quoteTotals->setItemsQty($quote->getItemsQty());
        $quoteTotals->setBaseCurrencyCode($quote->getBaseCurrencyCode());
        $quoteTotals->setQuoteCurrencyCode($quote->getQuoteCurrencyCode());
        return $quoteTotals;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::getCartMaskedId()
     */
    public function getCartMaskedId($quoteId)
    {
        if (is_numeric($quoteId)) {
            $quoteIdMask = $this->objectManager->create('Magento\Quote\Model\QuoteIdMask');
            $quoteIdMask->load($quoteId, 'quote_id');
            $maskedId = $quoteIdMask->getMaskedId();
        } else {
            $maskedId = $quoteId;
        }

        return $maskedId;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::createCart()
     */
    public function createCart()
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        // create the cart
        $customerId = null;
        if (!empty($bodyParams['customer_id'])) {
            $customerId = $bodyParams['customer_id'];
        }

        $quoteFactory = $this->objectManager->create('Magento\Quote\Model\QuoteFactory');
        $quote = $quoteFactory->create();
        $quoteResource = $quote->getResource();
        $quoteResource->getConnection()->beginTransaction();

        $quoteId = $this->createCartGeneric($customerId);
        $quoteResource->getConnection()->commit();

        if (!is_null($quoteId)) {

            $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
            $quote = $quoteInterceptor->get($quoteId);

            // remove items from cart (it's for the customer cart, createEmptyCartForCustomer() does not return an empty cart everytimes)
            $quoteItems = $quote->getItems();
            foreach ($quoteItems as $item) {
                $quote->removeItem($item->getId());
            }

            // store cart into DB
            $quoteInterceptor->save($quote);

            return $this->getCart($quoteId);
        }

        return new \Tapbuy\Checkout\Api\Data\TapbuyCart();
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::createCartWithProducts()
     */
    public function createCartWithProducts()
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        // 1- create the cart
        $customerId = null;
        if (!empty($bodyParams['customer_id'])) {
            $customerId = $bodyParams['customer_id'];
        }

        $quoteId = $this->createCartGeneric($customerId);

        if (!is_null($quoteId)) {

            // 2- add the product to the cart
            $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
            $quote = $quoteInterceptor->get($quoteId);

            // remove items from cart (it's for the customer cart, createEmptyCartForCustomer() does not return an empty cart everytime)
            $quoteItems = $quote->getItems();
            foreach ($quoteItems as $item) {
                $quote->removeItem($item->getId());
            }

            // add items to cart
            $quoteItems = [];
            foreach ($bodyParams['cartItem'] as $item) {
                $quoteItem = $this->objectManager->create('Magento\Quote\Model\Quote\Item');
                $quoteItem->setData('sku', $item['sku']);
                $quoteItem->setQty($item['qty']);
                $quoteItem->setData('quote_id', $quoteId);
                $quoteItems[] = $quoteItem;
            }

            $quote->setItems($quoteItems);

            // store cart into DB
            $quoteInterceptor->save($quote);

            // 3- add the default shipping
            if (!empty($bodyParams['addressInformation'])) {
                $shippingInfo = $this->objectManager->create('Magento\Checkout\Model\ShippingInformation');

                // set the shipping info, during create it's just country_id
                if (!empty($bodyParams['addressInformation']['shipping_address'])) {
                    $addressInterceptor = $this->objectManager->create('Magento\Quote\Model\Quote\Address\Interceptor');
                    $addressInterceptor->setCountryId($bodyParams['addressInformation']['shipping_address']['country_id']);

                    $shippingInfo->setShippingAddress($addressInterceptor);
                }

                // set the shipping carrier
                if (!empty($bodyParams['addressInformation']['shipping_carrier_code'])) {
                    $shippingInfo->setShippingCarrierCode($bodyParams['addressInformation']['shipping_carrier_code']);
                }

                // set the shipping method
                if (!empty($bodyParams['addressInformation']['shipping_method_code'])) {
                    $shippingInfo->setShippingMethodCode($bodyParams['addressInformation']['shipping_method_code']);
                }

                // save the shipping info for the cart into DB
                $shippingInfoManagement = $this->objectManager->create('Magento\Checkout\Model\ShippingInformationManagement\Interceptor');
                $shippingInfoManagement->saveAddressInformation($quoteId, $shippingInfo);
            }

            return $this->getCart($quoteId);
        }

        return new \Tapbuy\Checkout\Api\Data\TapbuyCart();
    }

    /**
     * generic function to create a guest cart or a cart for a customer
     *
     * @param string $customerId
     * @return NULL | int
     */
    protected function createCartGeneric($customerId = null)
    {

        // 1- create the cart
        if (!is_null($customerId)) {
            // the customer is logged, we need to get his cart (there is always a cart assigned to a customer)

            $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteManagement\Interceptor');
            $quoteId = $quoteInterceptor->createEmptyCartForCustomer($customerId); // this function return the active cart of the customer so if there are some items set they are not deleted
        } else {
            // it's a guest user, we create an empty guest cart
            $guestCartInterceptor = $this->objectManager->create('Magento\Quote\Model\GuestCart\GuestCartManagement\Interceptor');
            $maskedCartId = $guestCartInterceptor->createEmptyCart();

            $quoteIdMask = $this->objectManager->create('Magento\Quote\Model\QuoteIdMask');
            $quoteIdMask->load($maskedCartId, 'masked_id');
            $quoteId = $quoteIdMask->getQuoteId();
        }

        return $quoteId;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::setCustomer()
     */
    public function setCustomer($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        $quoteRessourceModel = $this->objectManager->create('Magento\Quote\Model\ResourceModel\Quote');

        // get the cart to check if it's not already assigned to the customer we want it to be assigned
        $tapbuyCart = $this->getCartCustomer($bodyParams['customer_id']);
        $customerCardId = $tapbuyCart->getCart()->getId();

        if ($customerCardId == $quoteId) {
            // the cart is already assigned to the customer, so we don't do anything
            return $tapbuyCart;
        }

        if (isset($bodyParams['no_merge']) && $bodyParams['no_merge']) {
            // we don't want to merge the customer cart with the guest cart, we have to remove all the items of the customer cart

            $quoteItems = $tapbuyCart->getCart()->getItems();
            foreach ($quoteItems as $item) {
                $tapbuyCart->getCart()->removeItem($item->getId());
            }

            if (!empty($quoteItems)) {
                $quoteRessourceModel->save($tapbuyCart->getCart());
            }
        }

        // before we assign the cart to customer, we have to deactivate it before
        $tapbuyCart->getCart()->setIsActive(false);
        $quoteRessourceModel->save($tapbuyCart->getCart());

        // assign the guest cart to a customer
        $quoteManagement = $this->objectManager->create('Magento\Quote\Model\QuoteManagement');
        $quoteManagement->assignCustomer($quoteId, $bodyParams['customer_id'], $bodyParams['store_id']);

        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::assignGuestCartProductToCustomerCart()
     */
    public function assignGuestCartProductToCustomerCart($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        $quoteRepository = $this->objectManager->create('Magento\Quote\Model\QuoteRepository');

        // get the cart to check if it's not already assigned to the customer we want it to be assigned
        $tapbuyCustomerCart = $this->getCartCustomer($bodyParams['customer_id']);
        $customerCardId = $tapbuyCustomerCart->getCart()->getId();

        if ($customerCardId == $quoteId) {
            // the cart is already assigned to the customer, so we don't do anything
            return $tapbuyCustomerCart;
        }

        // get the current product of the customer cart
        $customerQuoteItems = $tapbuyCustomerCart->getCart()->getItems();

        if (isset($bodyParams['no_merge']) && $bodyParams['no_merge']) {
            // we don't want to merge the customer cart with the guest cart, we have to remove all the items of the customer cart
            foreach ($customerQuoteItems as $item) {
                $tapbuyCustomerCart->getCart()->removeItem($item->getId());
            }

            $customerQuoteItems = [];

            if (!empty($customerQuoteItems)) {
                $quoteRepository->save($tapbuyCustomerCart->getCart());
            }
        }

        // get all product of the guest card
        $tapbuyGuestCart = $this->getCart($quoteId);
        $items = $tapbuyGuestCart->getCart()->getItems();

        // empty the guest cart and make it disabled
        foreach ($items as $item) {
            $tapbuyGuestCart->getCart()->removeItem($item->getId());
        }

        $tapbuyGuestCart->getCart()->setIsActive(false);
        $quoteRepository->save($tapbuyGuestCart->getCart());

        // add the product to customer cart and save
        $existingSkuList = [];
        if (!empty($customerQuoteItems)) {
            foreach ($customerQuoteItems as $customerQuoteItem) {
                $existingSkuList[] = $customerQuoteItem->getSku();
            }
        }

        foreach ($items as $item) {
            $index = array_search($item['sku'], $existingSkuList);

            if ($index === false) {
                // the product is not already in the cart, we have to add it
                $quoteItem = $this->objectManager->create('Magento\Quote\Model\Quote\Item');
                $quoteItem->setSku($item->getSku());
                $quoteItem->setQty($item->getQty());
                $quoteItem->setQuoteId($customerCardId);
                $customerQuoteItems[] = $quoteItem;
            } else {
                // the product is already in the cart, we just have to increase le quantity
                $newQty = $item['qty'] + $customerQuoteItems[$index]->getQty();
                $customerQuoteItems[$index]->setQty($newQty);
            }
        }

        $tapbuyCustomerCart->getCart()->setItems($customerQuoteItems);
        $quoteRepository->save($tapbuyCustomerCart->getCart());

        return $this->getCart($customerCardId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::getCartCustomer()
     */
    public function getCartCustomer($customerId)
    {
        $quoteFactory = $this->objectManager->create('Magento\Quote\Model\QuoteFactory');
        $quote = $quoteFactory->create();
        $quoteResource = $quote->getResource();
        $quoteResource->getConnection()->beginTransaction();

        try {
            // get customer cart id
            $quoteId = $this->getCartIdCustomer($customerId);
            $quote = $quoteFactory->create()->load($quoteId);
            $quoteId = $quote->getId();
        } catch (\Exception $e) {
            // do nothing
        }

        $quoteResource->getConnection()->commit();

        // get cart data
        return $this->getCart($quoteId);
    }

    /**
     * function to get the active cart id for a customer
     *
     * @param int $customerId
     * @return int
     */
    protected function getCartIdCustomer($customerId)
    {

        // get customer cart id
        $quoteManagement = $this->objectManager->create('Magento\Quote\Model\QuoteManagement\Interceptor');

        return $quoteManagement->createEmptyCartForCustomer($customerId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::deleteProducts()
     */
    public function deleteProducts($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        if (!empty($bodyParams['item_ids'])) {

            // get cart
            $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
            $quote = $quoteInterceptor->get($quoteId);

            // remove items from cart
            $quoteItems = $quote->getAllItems();
            $doSave = false;
            foreach ($quoteItems as $item) {
                if (in_array($item->getId(), $bodyParams['item_ids'])) {
                    $quote->removeItem($item->getId());
                    $doSave = true;
                }
            }

            // store cart into DB after items removal
            if ($doSave) {
                $quoteInterceptor->save($quote);
            }
        }

        // get cart data
        return $this->getCart($quote);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::addProducts()
     */
    public function addProducts($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        if (!empty($bodyParams['cart_items'])) {

            // get cart
            $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
            $quote = $quoteInterceptor->get($quoteId);

            $quoteItems = $quote->getItems();

            // get the existing product sku already in the cart
            $existingSkuList = [];
            if (!empty($quoteItems)) {
                foreach ($quoteItems as $origItem) {
                    $existingSkuList[] = $origItem->getSku();
                }
            }

            // add items to cart
            foreach ($bodyParams['cart_items'] as $item) {
                $index = array_search($item['sku'], $existingSkuList);

                if ($index === false) {
                    // the product is not already in the cart, we have to add it
                    $quoteItem = $this->objectManager->create('Magento\Quote\Model\Quote\Item');
                    $quoteItem->setData('sku', $item['sku']);
                    $quoteItem->setQty($item['qty']);
                    $quoteItem->setData('quote_id', $quoteId);
                    $quoteItems[] = $quoteItem;
                } else {
                    // the product is already in the cart, we just have to increase le quantity
                    $newQty = $item['qty'] + $quoteItems[$index]->getQty();
                    $quoteItems[$index]->setQty($newQty);
                }
            }

            $quote->setItems($quoteItems);

            // store cart into DB after items added
            $quoteInterceptor->save($quote);
        }

        // get cart data
        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::updateProduct()
     */
    public function updateProduct($quoteId, $itemId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        if (!empty($bodyParams['sku'])) {
            // get cart
            $quoteInterceptor = $this->objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
            $quote = $quoteInterceptor->get($quoteId);

            $origItem = $quote->getItemById($itemId);

            $action = 'none';
            if ($origItem) {
                if ($origItem->getSku() == $bodyParams['sku']) {
                    // it's just a quantity modification
                    $action = "update";
                } else {
                    // it's another product variation
                    $action = "replace";
                }
            }

            if ($action == 'update') {
                // it's just a quantity update
                $origItem->setQty($bodyParams['qty']);

                if ($this->retailerName == 'viapresse') {
                    $viapresseCart = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseCart');
                    $origItem = $viapresseCart->updateProduct($origItem, $bodyParams);
                }

                $quote->addItem($origItem);

                // need to re-collect totals before saving
                $quote->setTriggerRecollect(1);
                $quote->setTotalsCollectedFlag(false);
                $quote->setIsActive(true);
                $quote->collectTotals();

                // store cart into DB after items added
                $quoteInterceptor->save($quote);
            } elseif ($action == 'replace') {
                // item has been found, we have to delete it en replace by the SKU passed tot the function
                $quote->deleteItem($origItem);

                // we add the new variation
                $quoteItems = $quote->getItems();

                $quoteItem = $this->objectManager->create('Magento\Quote\Model\Quote\Item');
                $quoteItem->setData('sku', $bodyParams['sku']);
                $quoteItem->setQty($bodyParams['qty']);
                $quoteItem->setData('quote_id', $quoteId);
                $quoteItems[] = $quoteItem;

                // need to re-collect totals before saving
                $quote->setTriggerRecollect(1);
                $quote->setTotalsCollectedFlag(false);
                $quote->setIsActive(true);
                $quote->collectTotals();

                $quote->setItems($quoteItems);

                // store cart into DB after items added
                $quoteInterceptor->save($quote);
            }

            if ($this->retailerName == 'viapresse') {
                $viapresseCart = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseCart');
                $viapresseCart->setShippingAddressesItems($quoteId);
            }
        }

        // get cart data
        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::addCoupon()
     */
    public function addCoupon($quoteId, $couponCode)
    {
        $couponManagement = $this->objectManager->create('Magento\Quote\Model\CouponManagement');
        $couponManagement->set($quoteId, $couponCode);

        // get cart data
        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::removeCoupon()
     */
    public function removeCoupon($quoteId)
    {
        $couponManagement = $this->objectManager->create('Magento\Quote\Model\CouponManagement');
        $couponManagement->remove($quoteId);

        // get cart data
        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::setShippingInformation()
     */
    public function setShippingInformation($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        $shippingManagement = $this->objectManager->create('Magento\Checkout\Model\ShippingInformationManagement\Interceptor');
        $shippingInfo = $this->objectManager->create('Magento\Checkout\Model\ShippingInformation');

        $paymentMethods = [];

        if (!empty($bodyParams['addressInformation'])) {
            $quoteRepository = $this->objectManager->create(!is_numeric($quoteId) ? 'Magento\Quote\Model\GuestCart\GuestCartRepository' : 'Magento\Quote\Model\QuoteRepository');
            $quote = $quoteRepository->get($quoteId);

            $scopeConfig = $this->objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');
            $checkoutMultipleEnabled = $scopeConfig->getValue('multishipping/options/checkout_multiple', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $quote->getStore());

            if (!empty($bodyParams['addressInformation']['shipping_items'])) {
                if (!$quote->getCustomer()->getId()) {
                    throw new \Exception('Customer required for multishipping', 403);
                }

                if (empty($checkoutMultipleEnabled)) {
                    throw new \Exception('Multishipping is disabled', 403);
                }

                $checkoutSession = $this->objectManager->create('Magento\Checkout\Model\Session');
                $checkoutSession->replaceQuote($quote);

                $customerSession = $this->objectManager->create('Magento\Customer\Model\Session');
                $customerSession->setCustomerId($quote->getCustomer()->getId());

                if ($this->retailerName == 'viapresse') {
                    $multishippingInterceptor = $this->objectManager->create('Viapresse\Multishipping\Model\Checkout\Type\Multishipping\Interceptor');
                } else {
                    $multishippingInterceptor = $this->objectManager->create('Magento\Multishipping\Model\Checkout\Type\Multishipping\Interceptor');
                }

                $multishippingInterceptor->setData('checkout_session', $checkoutSession);

                $quote->setIsMultiShipping(count($bodyParams['addressInformation']['shipping_items']) > 1);

                // shipping addresses
                $shippingItemsInfo = [[]];
                foreach ($bodyParams['addressInformation']['shipping_items'] as $shippingItemId => $shippingItemData) {
                    $shippingItemsInfo[0][$shippingItemId] = [
                        'address' => $shippingItemData['address_id'],
                        'qty' => !empty($shippingItemData['quantity']) ? $shippingItemData['quantity'] : 1
                    ];
                }

                if (!empty($shippingItemsInfo)) {
                    $multishippingInterceptor->setShippingItemsInformation($shippingItemsInfo);

                    $shippingMethodsInfo = [];
                    foreach ($bodyParams['addressInformation']['shipping_items'] as $shippingItemData) {
                        $quoteShippingAddress = $quote->getShippingAddressByCustomerAddressId($shippingItemData['address_id']);

                        if (!empty($quoteShippingAddress)) {
                            $shippingMethodsInfo[$quoteShippingAddress->getId()] = $shippingItemData['method'];
                        }
                    }

                    if (!empty($shippingMethodsInfo)) {
                        $multishippingInterceptor->setShippingMethods($shippingMethodsInfo);
                    }
                }

                // billing address
                if (!empty($bodyParams['addressInformation']['billing_address'])) {
                    $billingAddress = $this->buildAddress($bodyParams['addressInformation']['billing_address']);
                    $quote->setBillingAddress($billingAddress);
                    $quote->save();
                }

                $paymentMethods = $this->objectManager->create('Magento\Quote\Api\PaymentMethodManagementInterface')->getList($quoteId);
            } else {
                $updateShippingInfo = false;
                // shipping address info
                if (!empty($bodyParams['addressInformation']['shipping_address'])) {
                    $shippingAddress = $this->buildAddress($bodyParams['addressInformation']['shipping_address']);
                    $shippingInfo->setShippingAddress($shippingAddress);
                    $updateShippingInfo = true;
                }

                // billing address info
                if (!empty($bodyParams['addressInformation']['billing_address'])) {
                    $billingAddress = $this->buildAddress($bodyParams['addressInformation']['billing_address']);
                    $shippingInfo->setBillingAddress($billingAddress);
                    $updateShippingInfo = true;
                }

                // set the carrier data
                if (!empty($bodyParams['addressInformation']['shipping_method_code']) && !empty($bodyParams['addressInformation']['shipping_carrier_code'])) {
                    $shippingInfo->setShippingMethodCode($bodyParams['addressInformation']['shipping_method_code']);
                    $shippingInfo->setShippingCarrierCode($bodyParams['addressInformation']['shipping_carrier_code']);
                    $updateShippingInfo = true;
                }

                // set shipping info to the cart
                if ($updateShippingInfo) {
                    // reset shipping addresses
                    if ($quote->isMultipleShippingAddresses()) {
                        $shippingAddresses = $quote->getAllShippingAddresses();
                        foreach ($shippingAddresses as $shippingAddress) {
                            $quote->removeAddress($shippingAddress->getId());
                        }
                    }

                    $wrongCountry = false;

                    if ($this->retailerName == 'viapresse') {
                        $quote->setIsMultiShipping(false);
                        $viapresseCart = $this->objectManager->create('Tapbuy\Checkout\Model\Viapresse\ViapresseCart');
                        $wrongCountry = $viapresseCart->resetAddressForUnavailableCountry($quote);
                    }
                    $quote->save();
                    if ($wrongCountry) {
                        $paymentDetailsFactory = $this->objectManager->create('Magento\Checkout\Model\PaymentDetailsFactory');
                        $paymentMethodManagement = $this->objectManager->create('Magento\Quote\Api\PaymentMethodManagementInterface');
                        $cartTotalsRepository = $this->objectManager->create('Magento\Quote\Api\CartTotalRepositoryInterface');
                        $paymentDetails = $paymentDetailsFactory->create();
                        $paymentDetails->setPaymentMethods($paymentMethodManagement->getList($quoteId));
                        $paymentDetails->setTotals($cartTotalsRepository->get($quoteId));
                    } else {
                        $paymentDetails = $shippingManagement->saveAddressInformation($quoteId, $shippingInfo);
                    }
                    $paymentMethods = $paymentDetails->getPaymentMethods();
                }
            }

            // specific code for some retailer
            if (!empty($bodyParams['retailer_specific'])) {
                if (!empty($bodyParams['retailer_specific']['retailer_id'])) {
                    $quote->setData('retailer_id', $bodyParams['retailer_specific']['retailer_id']);
                    $quote->save();
                }
            }
        }

        // get cart data
        $tapbuyCart = $this->getCart($quoteId);
        $tapbuyCart->setCartPaymentMethods($paymentMethods);

        return $tapbuyCart;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::setBillingAddress()
     */
    public function setBillingAddress($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        if (!empty($bodyParams['address'])) {
            $billingAddress = $this->buildAddress($bodyParams['address']);
            $billingAddressManagement = $this->objectManager->create('Magento\Quote\Model\BillingAddressManagement');
            $billingAddressManagement->assign($quoteId, $billingAddress, false);
        }

        // get cart data
        return $this->getCart($quoteId);
    }

    /**
     * function to build an address object
     *
     * @param array $addressData
     * @return \Magento\Quote\Api\Data\AddressInterface
     */
    public function buildAddress(array $addressData = [])
    {
        $address = $this->objectManager->create('Magento\Quote\Model\Quote\Address\Interceptor');

        if (!empty($addressData)) {
            if (!empty($addressData['firstname'])) {
                $address->setFirstname($addressData['firstname']);
            }

            if (!empty($addressData['lastname'])) {
                $address->setLastname($addressData['lastname']);
            }

            if (!empty($addressData['middlename'])) {
                $address->setMiddlename($addressData['middlename']);
            }

            if (!empty($addressData['prefix'])) {
                $address->setPrefix($addressData['prefix']);
            }

            if (!empty($addressData['suffix'])) {
                $address->setSuffix($addressData['suffix']);
            }

            if (!empty($addressData['email'])) {
                $address->setEmail($addressData['email']);
            }

            if (!empty($addressData['company'])) {
                $address->setCompany($addressData['company']);
            }

            if (!empty($addressData['street'])) {
                $address->setStreet($addressData['street']);
            }

            if (!empty($addressData['postcode'])) {
                $address->setPostcode($addressData['postcode']);
            }

            if (!empty($addressData['city'])) {
                $address->setCity($addressData['city']);
            }

            if (!empty($addressData['region'])) {
                $address->setRegion($addressData['region']);
            }

            if (!empty($addressData['region_id'])) {
                $address->setRegionId($addressData['region_id']);
            }

            if (!empty($addressData['country_id'])) {
                $address->setCountryId($addressData['country_id']);
            }

            if (!empty($addressData['telephone'])) {
                $address->setTelephone($addressData['telephone']);
            }

            if (!empty($addressData['fax'])) {
                $address->setFax($addressData['fax']);
            }

            if (!empty($addressData['vat_id'])) {
                $address->setVatId($addressData['vat_id']);
            }

            if (!empty($addressData['customer_id'])) {
                $address->setCustomerId($addressData['customer_id']);
            }

            if (!empty($addressData['same_as_billing'])) {
                $address->setSameAsBilling($addressData['same_as_billing']);
            }

            if (!empty($addressData['customer_address_id'])) {
                $address->setCustomerAddressId($addressData['customer_address_id']);
            }

            if (!empty($addressData['save_in_address_book'])) {
                $address->setSaveInAddressBook($addressData['save_in_address_book']);
            }

            if (!empty($addressData['extension_attributes'])) {
                // TODO
            }
        }

        return $address;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::unlock()
     */
    public function unlock($quoteId)
    {
        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        $orderFactory = $this->objectManager->create('\Magento\Sales\Model\OrderFactory');

        $order = $orderFactory->create();
        $orderResource = $order->getResource();

        // Begin transaction to lock this order record during update
        $orderResource->getConnection()->beginTransaction();

        // load the order
        $order->getResource()->load($order, $quoteId, 'quote_id');

        if ($order->getId()) {

            // definition of the unlock reason
            $msg_txt = "Tapbuy Unlock: ";
            if (isset($bodyParams['unlock_reason']) && $bodyParams['unlock_reason'] == 'cancel') {
                $configDataKey = "order_status_payment_canceled";
                $msg_txt .= "payment canceled";
            } else {
                $configDataKey = "order_status_payment_refused";
                $msg_txt .= "payment refused";
            }

            // set message to order
            $message = $order->addStatusHistoryComment($msg_txt);
            $message->setIsCustomerNotified(null);

            try {
                $paymentMethodInstance = $order->getPayment()->getMethodInstance();
                $orderStatus = $paymentMethodInstance->getConfigData($configDataKey);
            } catch (\Exception $e) {
                $orderStatus = 'canceled';
            }

            // set the status and save the order
            $order->setStatus($orderStatus)->save();
        }

        // reactivate the cart
        $quote = $this->getCartGeneric($quoteId);

        if ($quote->getID()) {
            $quote->setIsActive(1)->setReservedOrderId(null)->save();
        }

        // Send commit to unlock tables
        $orderResource->getConnection()->commit();

        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::unlockNew()
     */
    public function unlockNew($quoteId)
    {
        // check if the cart is not already active
        $quote = $this->getCartGeneric($quoteId);

        if ($quote->getIsActive()) {
            return $this->getCart($quoteId);
        }

        // get the body param of the request
        $bodyParams = $this->_request->getBodyParams();

        // check the PSP param is provided to know which code we need to execute
        if (!isset($bodyParams['method'])) {
            throw new \Exception('No method provided', 404);
        } elseif ($bodyParams['method'] != 'hipay') {
            throw new \Exception('Invalid method', 404);
        }

        $orderCollectionFactory = $this->objectManager->create('\Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
        $orderCollection = $orderCollectionFactory->create()
        ->addAttributeToSelect('*')
        ->addFieldToFilter('quote_id', ['in' => $quoteId]);

        $dontDoUnlock = false;
        if ($orderCollection->getSize() > 0) {
            if ($bodyParams['method'] == 'hipay') {
                // definition of the unlock reason
                $msg_txt = "Tapbuy Unlock: ";
                if (isset($bodyParams['unlock_reason']) && $bodyParams['unlock_reason'] == 'cancel') {
                    $configDataKey = "order_status_payment_canceled";
                    $msg_txt .= "payment canceled";
                } else {
                    $configDataKey = "order_status_payment_refused";
                    $msg_txt .= "payment refused";
                }

                $statuses = [];
                if (class_exists('\HiPay\Fullservice\Enum\Transaction\TransactionState')) {
                    $statuses[] = \HiPay\Fullservice\Enum\Transaction\TransactionState::DECLINED;
                    $statuses[] = \HiPay\Fullservice\Enum\Transaction\TransactionState::ERROR;
                }

                // check if we have some orders without payment problem, in this case we don't unlock
                foreach ($orderCollection as $order) {
                    $paymentStatus = $order->getPayment()->getAdditionalInformation('status');
                    if (!in_array($paymentStatus, $statuses)) {
                        $dontDoUnlock = true;
                    } else {
                        try {
                            $paymentMethodInstance = $order->getPayment()->getMethodInstance();
                            $orderStatus = $paymentMethodInstance->getConfigData($configDataKey);
                        } catch (\Exception $e) {
                            $orderStatus = 'canceled';
                        }

                        $order->getResource()->getConnection()->beginTransaction();

                        // set message to order
                        $message = $order->addStatusHistoryComment($msg_txt);
                        $message->setIsCustomerNotified(null);

                        // set the status and save the order
                        $order->setStatus($orderStatus)->save();

                        // Send commit to unlock tables
                        $order->getResource()->getConnection()->commit();
                    }
                }
            }
        }

        if ($dontDoUnlock) {
            // Send commit to unlock tables
            throw new \Exception('Unlock impossible', 403);
        } else {
            // reactivate the cart
            $quote = $this->getCartGeneric($quoteId);

            if ($quote->getID()) {
                $quote->setIsActive(1)->setReservedOrderId(null)->save();
            }

            return $this->getCart($quoteId);
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::applyBalance()
     */
    public function applyBalance($quoteId)
    {
        $customerBalanceHelper = $this->objectManager->create('Magento\CustomerBalance\Helper\Data');

        $quote = $this->getCartGeneric($quoteId);

        if ($customerBalanceHelper->isEnabled() && $quote->getUseCustomerBalance() !== '1') {
            $quote->setUseCustomerBalance(true)->collectTotals()->save();
        }

        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::removeBalance()
     */
    public function removeBalance($quoteId)
    {
        $customerBalanceHelper = $this->objectManager->create('Magento\CustomerBalance\Helper\Data');

        $quote = $this->getCartGeneric($quoteId);

        if ($customerBalanceHelper->isEnabled() && $quote->getUseCustomerBalance() === '1') {
            $quote->setUseCustomerBalance(false)->collectTotals()->save();
        }

        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::applyGiftCard()
     */
    public function applyGiftCard($quoteId, $giftCardCode)
    {
        $giftCardModule = $this->_request->getHeader('X-Tapbuy-Gift-Card-Module', false);

        if ($giftCardModule == 'amasty') {
            $this->applyGiftCardAmasty($quoteId, $giftCardCode);
        } elseif ($giftCardModule == 'viapresse') {
            $this->applyGiftCardViapresse($quoteId, $giftCardCode);
        } else {
            throw new \Exception('Module not supported', 404);
        }

        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::removeGiftCard()
     */
    public function removeGiftCard($quoteId, $giftCardCode)
    {
        $giftCardModule = $this->_request->getHeader('X-Tapbuy-Gift-Card-Module', false);

        if ($giftCardModule == 'amasty') {
            $this->removeGiftCardAmasty($quoteId, $giftCardCode);
        } elseif ($giftCardModule == 'viapresse') {
            $this->removeGiftCardViapresse($quoteId, $giftCardCode);
        } else {
            throw new \Exception('Module not supported', 404);
        }

        return $this->getCart($quoteId);
    }

    /**
     * apply gift card function from Amasty module
     * @param string $cartId
     * @param string $giftCardCode
     */
    protected function applyGiftCardAmasty($quoteId, $giftCardCode)
    {
        if (!is_numeric($quoteId)) {
            $quoteId = $this->getCartIdFromMaskedId($quoteId);
        }

        try {
            $amastyGiftCardAccountManagement = $this->objectManager->create('Amasty\GiftCardAccount\Model\GiftCardAccount\GiftCardAccountManagement');
            return $amastyGiftCardAccountManagement->applyGiftCardToCart($quoteId, $giftCardCode);
        } catch (CouldNotSaveException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new \Exception('Module Amasty not installed', 404);
        }
    }

    /**
     * apply gift card function from Viapresse Module
     * @param string $cartId
     * @param string $giftCardCode
     */
    protected function applyGiftCardViapresse($quoteId, $giftCardCode)
    {

        $quote = $this->getCartGeneric($quoteId);
        $haveGiftcard = false;

        $viaGiftHelper = $this->objectManager->create('Viapresse\GiftProduct\ViewModel\GiftProduct');
        foreach ($quote->getItems() as $item) {
            if ($viaGiftHelper->isGiftcardProduct($item->getSku())) {
                $haveGiftcard = true;
            }
        }

        if ($haveGiftcard && (int)$quote->getItemsCount() === 1) {
            throw new \Exception(__('We cannot apply this gift card.'), 404);
        } elseif ($giftCardCode) {
            $giftCardAccountFactory = $this->objectManager->create('Viapresse\GiftCardAccount\Api\Data\GiftCardAccountInterfaceFactory');
            $giftCardAccountManagement = $this->objectManager->create('Viapresse\GiftCardAccount\Api\GiftCardAccountManagementInterface');
            $giftCardAccount = $giftCardAccountFactory->create();
            $giftCardAccount->setGiftCards([$giftCardCode]);

            try {
                $giftCardAccountManagement->saveByQuoteId(
                    $quote->getId(),
                    $giftCardAccount
                );
            } catch (\Viapresse\GiftCardAccount\Api\Exception\TooManyAttemptsException $e) {
                throw new \Exception($e->getMessage(), 404);
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                throw new \Exception($e->getMessage(), 404);
            } catch (\Exception $e) {
                throw new \Exception(__('We cannot apply this gift card.'), 404);
            }
        }

        return $this->getCart($quoteId);
    }

    /**
     * remove gift card function from Amasty module
     * @param string $cartId
     * @param string $giftCardCode
     */
    protected function removeGiftCardAmasty($quoteId, $giftCardCode)
    {
        if (!is_numeric($quoteId)) {
            $quoteId = $this->getCartIdFromMaskedId($quoteId);
        }

        try {
            $amastyGiftCardAccountManagement = $this->objectManager->create('Amasty\GiftCardAccount\Model\GiftCardAccount\GiftCardAccountManagement');
            return $amastyGiftCardAccountManagement->removeGiftCardFromCart($quoteId, $giftCardCode);
        } catch (CouldNotDeleteException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw new \Exception('Module Amasty not installed', 404);
        }
    }

    /**
     * remove gift card function from Viapresse module
     * @param string $cartId
     * @param string $giftCardCode
     */
    protected function removeGiftCardViapresse($quoteId, $giftCardCode)
    {
        $quote = $this->getCartGeneric($quoteId);

        if ($giftCardCode) {
            $giftCardAccountManagement = $this->objectManager->create('Viapresse\GiftCardAccount\Api\GiftCardAccountManagementInterface');
            try {
                $giftCardAccountManagement->deleteByQuoteId(
                    $quote->getId(),
                    $giftCardCode
                );
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                throw new \Exception($e->getMessage(), 404);
            } catch (\Exception $e) {
                throw new \Exception(__('You can\'t remove this gift card.'), 404);
            }
        }

        return $this->getCart($quoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::duplicate()
     */
    public function duplicate($quoteId)
    {
        // get cart data
        $quote = $this->getCartGeneric($quoteId);

        if ($quote->getId()) {
            $bodyParams = $this->_request->getBodyParams();

            $quoteFactory = $this->objectManager->create('Magento\Quote\Model\QuoteFactory');

            // get customer from the cart
            $hasCustomer = false;
            $customer = $quote->getCustomer();

            if ($customer->getId()) {
                $hasCustomer = true;
            }

            // get all possible options
            $deactivateSource = false;
            if (isset($bodyParams['deactivate_source']) && $bodyParams['deactivate_source'] == 1) {
                $deactivateSource = true;
            }

            $customerCartReuse = false;
            if (isset($bodyParams['customer_cart_reuse']) && $bodyParams['customer_cart_reuse'] == 1) {
                $customerCartReuse = true;
            }

            $keepCoupon = false;
            if (isset($bodyParams['keep_coupon']) && $bodyParams['keep_coupon'] == 1) {
                $keepCoupon = true;
            }

            $keepGiftCard = false;
            if (isset($bodyParams['keep_gift_card']) && $bodyParams['keep_gift_card'] == 1) {
                $keepGiftCard = true;
            }

            $keepBillingAddress = false;
            if (isset($bodyParams['keep_billing_address']) && $bodyParams['keep_billing_address'] == 1) {
                $keepBillingAddress = true;
            }

            $keepShippingAddress = false;
            if (isset($bodyParams['keep_shipping_address']) && $bodyParams['keep_shipping_address'] == 1) {
                $keepShippingAddress = true;
            }

            // for some customer we reuse the existing customer cart
            if ($hasCustomer && $customerCartReuse) {
                $customerCardId = $this->getCartIdCustomer($customer->getId());

                if ($customerCardId == $quote->getId()) {
                    throw new \Exception('Fobiden, same cart ID', 403);
                }

                $newQuote = $quoteFactory->create()->load($customerCardId);

                // need to delete the existing items
                $customerCartItems = $newQuote->getItemsCollection();

                foreach ($customerCartItems as $item) {
                    $newQuote->removeItem($item->getId());
                }
            } else {
                // create empty new cart
                /** @var \Magento\Quote\Api\Data\CartInterface $newQuote */
                $newQuote = $quoteFactory->create();
            }

            // set store
            $newQuote->setStoreId($quote->getStoreId());

            $quoteItems = $quote->getItemsCollection();

            $productFactory = $this->objectManager->create('Magento\Catalog\Model\ProductFactory');

            // add products to new cart
            foreach ($quoteItems as $item) {
                $productId = $item->getProductId();
                $product = $productFactory->create()->load($productId);

                $options = $item->getProduct()->getTypeInstance(true)->getOrderOptions($item->getProduct());

                $info = $options['info_buyRequest'];
                $request1 = new \Magento\Framework\DataObject();
                $request1->setData($info);

                $newQuote->addProduct($product, $request1);
            }

            // get coupon code
            $couponCode = $quote->getCouponCode();

            // get gift card ???
            if ($keepGiftCard) {
                $giftCardCodeList = [];
                $giftCardModule = $this->_request->getHeader('X-Tapbuy-Gift-Card-Module', false);
                if ($giftCardModule == 'amasty') {
                    $giftCardCodeList = $this->getGiftCardsQuoteAmasty($quoteId);
                }
            }

            // add billing address
            if ($keepBillingAddress) {
                $billingAddress = $quote->getBillingAddress();
                if ($billingAddress->getCustomerId()) {
                    $newBillingAddress = $billingAddress;
                } else {
                    $newBillingAddress = $this->duplicateAddressObject($billingAddress);
                }

                $newQuote->setBillingAddress($newBillingAddress);
            }

            $newQuote->setTriggerRecollect(1);
            $newQuote->setTotalsCollectedFlag(false);
            $newQuote->setIsActive(true);
            $newQuote->collectTotals();

            $newQuote->save();
            $newQuoteId = $newQuote->getId();

            // add customer
            if ($hasCustomer && !$customerCartReuse) {

                // the cart we are duplicating belongs to customer, to be able to duplicate it and assign the duplicated cart to customer, we have to deactivate the cart we are duplicating
                if ($quote->getIsActive()) {
                    $quote->setIsActive(false);
                    $quote->save();
                }

                $quoteManagement = $this->objectManager->create('Magento\Quote\Model\QuoteManagement');
                $quoteManagement->assignCustomer($newQuoteId, $customer->getId(), $customer->getStoreId());
            }

            // add shipping address
            if ($keepShippingAddress) {
                $shippingAddress = $quote->getShippingAddress();

                if ($shippingAddress->getCustomerId()) {
                    $newShippingAddress = $shippingAddress;
                } else {
                    $newShippingAddress = $this->duplicateAddressObject($shippingAddress);
                }

                /** @var ShippingInformationInterface $shippingInfo */
                $shippingInfo = $this->objectManager->create('Magento\Checkout\Model\ShippingInformation');
                $shippingInfo->setShippingAddress($newShippingAddress);

                /** @var \Magento\Quote\Model\ShippingMethodManagement $shippingMethodManagement */
                $shippingMethodManagement = $this->objectManager->create('Magento\Quote\Model\ShippingMethodManagement');

                // because it's impossible to get current shipping method on inactive cart, we get all possible shipping
                // and try to determine the correct one from string coming from $shippingAddress->getShippingMethod()
                $shippingMethods = $shippingMethodManagement->estimateByExtendedAddress($newQuoteId, $newShippingAddress);

                $saveShippingInfo = false;
                if (!empty($shippingMethods)) {
                    foreach ($shippingMethods as $shippingMethod) {
                        $method =  $shippingMethod->getCarrierCode() . "_" . $shippingMethod->getMethodCode();
                        if ($method == $shippingAddress->getShippingMethod()) {
                            $shippingInfo->setShippingMethodCode($shippingMethod->getMethodCode());
                            $shippingInfo->setShippingCarrierCode($shippingMethod->getCarrierCode());
                            $saveShippingInfo = true;
                            break;
                        }
                    }

                    if (!$saveShippingInfo && !empty($shippingMethods)) {
                        // no shipping method seems to match, we use the first in the list
                        $shippingInfo->setShippingMethodCode($shippingMethods[0]->getMethodCode());
                        $shippingInfo->setShippingCarrierCode($shippingMethods[0]->getCarrierCode());
                        $saveShippingInfo = true;
                    }
                }

                if ($saveShippingInfo) {
                    /** @var  \Magento\Checkout\Model\ShippingInformationManagement $shippingManagement */
                    $shippingManagement = $this->objectManager->create('Magento\Checkout\Model\ShippingInformationManagement');

                    // call the getCart otherwise the saveAddressInformation() call is dumping for no item in cart
                    $newCart = $this->getCartGeneric($newQuoteId);

                    // set shipping info to the cart
                    $paymentDetails = $shippingManagement->saveAddressInformation($newQuoteId, $shippingInfo);
                }
            }

            // add coupon code if any
            if ($keepCoupon && !empty($couponCode)) {
                $couponManagement = $this->objectManager->create('Magento\Quote\Model\CouponManagement');

                try {
                    $couponManagement->set($newQuoteId, $couponCode);
                } catch (\Exception $e) {
                    // do nothing
                    // we don't want to block the process for a coupon
                }
            }

            // add giftCard if any ???
            if ($keepGiftCard) {
                $giftCardModule = $this->_request->getHeader('X-Tapbuy-Gift-Card-Module', false);
                if ($giftCardModule == 'amasty') {
                    foreach ($giftCardCodeList as $giftCardCode) {
                        try {
                            $this->applyGiftCardAmasty($newQuoteId, $giftCardCode);
                        } catch (\Exception $e) {
                            // nothing to do
                        }
                    }
                }
            }

            // inactivate the duplicated cart if requested
            if ($deactivateSource) {
                $quote->setIsActive(false);
                $quote->save();
            }
        }

        return $this->getCart($newQuoteId);
    }

    /**
     *
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CartInterface::duplicate()
     */
    public function deactivate($quoteId)
    {
        // get cart data
        $quote = $this->getCartGeneric($quoteId);

        if ($quote->getId()) {
            $quote->setIsActive(false);
            $quote->save();
        } else {
            throw new \Exception('Cart not found', 404);
        }

        return ['success' => true];
    }

    /**
     * duplicate an address into anothe address object
     * the new address is not save to DB
     *
     * @param AddressInterface $address
     * @param bool $saveToDb
     * @return \Magento\Quote\Api\Data\AddressInterface
     */
    protected function duplicateAddressObject(AddressInterface $address)
    {
        $addressFactory = $this->objectManager->create('Magento\Quote\Model\Quote\AddressFactory');

        /** @var \Magento\Quote\Api\Data\AddressInterface $newAddress */
        $newAddress = $addressFactory->create();

        $newAddress->setEmail($address->getEmail());
        $newAddress->setCountryId($address->getCountryId());
        $newAddress->setRegionId($address->getRegionId());
        $newAddress->setRegionCode($address->getRegionCode());
        $newAddress->setRegion($address->getRegion());
        $newAddress->setCustomerId($address->getCustomerId());
        $newAddress->setStreet($address->getStreet());
        $newAddress->setCompany($address->getCompany());
        $newAddress->setTelephone($address->getTelephone());
        $newAddress->setFax($address->getFax());
        $newAddress->setPostcode($address->getPostcode());
        $newAddress->setCity($address->getCity());
        $newAddress->setFirstname($address->getFirstname());
        $newAddress->setLastname($address->getLastname());
        $newAddress->setMiddlename($address->getMiddlename());
        $newAddress->setPrefix($address->getPrefix());
        $newAddress->setSuffix($address->getSuffix());
        $newAddress->setVatId($address->getVatId());
        $newAddress->setSameAsBilling($address->getSameAsBilling());
        $newAddress->setCustomerAddressId($address->getCustomerAddressId());
        $newAddress->setSaveInAddressBook($address->getSaveInAddressBook());
        $newAddress->setExtensionAttributes($address->getExtensionAttributes());
        $newAddress->setCustomAttributes($address->getCustomAttributes());

        return $newAddress;
    }

    /**
     * get the list of gift card assigned to quote
     *
     * @param string $quoteId
     * @return array
     */
    protected function getGiftCardsQuoteAmasty($quoteId)
    {
        $amastyCodeList = [];
        $quote = $this->getCartGeneric($quoteId);

        if ($quote->getId()) {
            try {
                if ($quote->getExtensionAttributes() && $quote->getExtensionAttributes()->getAmGiftcardQuote()) {
                    $cards = $quote->getExtensionAttributes()->getAmGiftcardQuote()->getGiftCards();
                }

                foreach ($cards as $k => $card) {
                    $amastyCodeList[] = $card[\Amasty\GiftCardAccount\Model\GiftCardAccount\GiftCardCartProcessor::GIFT_CARD_CODE];
                }
            } catch (\Exception $e) {
                // we do nothing here
            } catch (\Throwable $e) {
                // we do nothing here
            }
        }

        return $amastyCodeList;
    }
}
