<?php

namespace Tapbuy\Checkout\Model;

use Tapbuy\Checkout\Api\CustomerInterface;
use Tapbuy\Checkout\Api\Data\TapbuyCustomer;
use Tapbuy\Checkout\Api\Data\TapbuyCustomerBalance;

class Customer implements CustomerInterface
{

    /**
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;

    /**
     * @param \Magento\Framework\Webapi\Rest\Request $request
     */
    public function __construct(\Magento\Framework\Webapi\Rest\Request $request)
    {
        $this->_request = $request;
    }

    /**
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CustomerInterface::getCustomer()
     */
    public function getCustomer($customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerRepository = $objectManager->create('Magento\Customer\Model\ResourceModel\CustomerRepository');

        $customer = $customerRepository->getById($customerId);

        $tapbuyCustomer = new TapbuyCustomer();
        $tapbuyCustomer->setCustomer($customer);

        $withCustomerBalance = $this->_request->getHeader('X-Tapbuy-Customer-Balance', false);

        if($withCustomerBalance) {
            $customerBalance = $this->getCustomerBalance($customerId);
            $tapbuyCustomer->setCreditBalance($customerBalance);
        }

        return $tapbuyCustomer;
    }

    /**
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CustomerInterface::getAlias()
     */
    public function getCardsAliases($customerId, $psp)
    {
        // we don't do anything here, we assume the card aliases are managed by PSP plugin
        // we get the aliases in the Tapbuy PSP specific module (ex: Tapbuy_PspPluginHipay) in an after plugin method
        return [];
    }

    /**
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CustomerInterface::addCardAlias()
     */
    public function addCardAlias($customerId)
    {
        // we don't do anything here, we assume the card aliases are managed by PSP plugin
        // we add the alias in the Tapbuy PSP specific module (ex: Tapbuy_PspPluginHipay) in an after plugin method
        return [];
    }

    /**
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CustomerInterface::deleteCardAlias()
     */
    public function deleteCardAlias($customerId, $cardAliasId)
    {
        // we don't do anything here, we assume the card aliases are managed by PSP plugin
        // we delete the alias in the Tapbuy PSP specific module (ex: Tapbuy_PspPluginHipay) in an after plugin method
        return [];
    }

    /**
     * {@inheritDoc}
     * @see \Tapbuy\Checkout\Api\CustomerInterface::getCustomerBalance()
     */
    public function getCustomerBalance($customerId)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->create('Magento\Store\Model\StoreManagerInterface');

        $websiteId = $this->_request->getHeader('X-Tapbuy-Website-Id', false);

        if(!$websiteId) {
            $websiteId = $storeManager->getStore()->getWebsiteId();
        }

        $balanceFactory = $objectManager->create('Magento\CustomerBalance\Model\BalanceFactory');
        $balanceModel = $balanceFactory->create()->setCustomerId($customerId)->setWebsiteId($websiteId)->loadByCustomer();

        $tapbuyCustomerBalance = new TapbuyCustomerBalance();

        $tapbuyCustomerBalance
            ->setCustomerId($customerId)
            ->setWebsiteId($websiteId)
            ->setAmount($balanceModel->getAmount())
            ->setBaseCurrencyCode($balanceModel->getBaseCurrencyCode());

        return $tapbuyCustomerBalance;
    }

}
