<?php

namespace Tapbuy\Checkout\Model;

use Tapbuy\Checkout\Api\ConfigurationInterface;
use Magento\Framework\App\Utility\Files;


class Configuration implements ConfigurationInterface
{
    protected $files;

    /**
     * @param Files $files
     */
    public function __construct(Files $files) {
        $this->files = $files;
    }

    public function getModuleVersion($appPath, $vendorPath)
    {
        $pathToNeededModule = '';

        $composerFilePaths = array_keys(
            $this->files->getComposerFiles(\Magento\Framework\Component\ComponentRegistrar::MODULE)
        );

        foreach ($composerFilePaths as $path) {

            //from app/code - may be specific logic
            if (strpos($path, $appPath . '/composer.json')) {
                $pathToNeededModule = '../' . $path;
                break;
            }

            //from vendor - may be specific logic
            if (strpos($path, $vendorPath . '/composer.json')) {
                $pathToNeededModule = '../' . $path;
                break;
            }
        }

        if ($pathToNeededModule) {
            $content = file_get_contents($pathToNeededModule);

            if ($content) {
                $jsonContent = json_decode($content, true);

                if (!empty($jsonContent['version'])) {
                    return $jsonContent['version'];
                }
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function checkModule()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        $modules = [
            'checkout' => $this->getModuleVersion('Tapbuy/Checkout', 'tapbuy/checkout'),
        ];
        $adyen = $this->getModuleVersion('Tapbuy/PspPluginAdyen', 'tapbuy/psppluginadyen');
        if (!is_null($adyen)) {
            $modules['adyen'] = $adyen;
        }
        $hipay = $this->getModuleVersion('Tapbuy/PspPluginHipay', 'tapbuy/psppluginhipay');
        if (!is_null($hipay)) {
            $modules['hipay'] = $hipay;
        }
        return [$modules];
    }
}
