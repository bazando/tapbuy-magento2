<?php

namespace Tapbuy\Checkout\Model\Paypal;

use Magento\Paypal\Model\AbstractConfig as PaypalAbstractConfig;
use Magento\Paypal\Model\Express\Checkout as ExpressCheckout;
use Magento\Sales\Model\Order;

class Express
{
    private \Magento\Framework\Webapi\Request $request;

    /**
     * constructor
     *
     * @param \Magento\Framework\Webapi\Request $request
     */
    public function __construct(
        \Magento\Framework\Webapi\Request $request
    ) {
        $this->request = $request;
    }

    private function getQuote()
    {
        if ($this->request->getHeader('X-Tapbuy-Call')) {
            $requestUri = $this->request->getRequestUri();
            if (preg_match('/carts\/(.*?)\/payment/', $requestUri, $match) == 1) {
                $cartId = $match[1];
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

                if (!is_numeric($cartId)) {
                    $quoteInterceptor = $objectManager->create('Magento\Quote\Model\GuestCart\GuestCartRepository');
                } else {
                    $quoteInterceptor = $objectManager->create('Magento\Quote\Model\QuoteRepository\Interceptor');
                }

                $quote = $quoteInterceptor->get($cartId);

                if ($quote) {
                    return $quote;
                }
            }
        }
        return false;
    }

    /**
     * redifine this check because in ViaPresse it's using the chekcout object to get the quote, but as there is no checkout in API mode we don't get a correct quote so the test done is not valid
     *
     * @param \Magento\Paypal\Model\Express $subject
     * @param  mixed $result
     * @return boolean
     */
    public function afterIsInitializeNeeded(\Magento\Paypal\Model\Express $subject, $result)
    {
        $quote = $this->getQuote();
        if ($quote) {
            $result = true;
        }
        return $result;
    }

    public function afterInitialize(
        \Magento\Paypal\Model\Express $subject,
        \Magento\Paypal\Model\Express $result,
        $paymentAction,
        $stateObject
    ) {
        $quote = $this->getQuote();
        if ($quote) {
            switch ($paymentAction) {
                case PaypalAbstractConfig::PAYMENT_ACTION_AUTH:
                case PaypalAbstractConfig::PAYMENT_ACTION_SALE:

                    $payment = $result->getInfoInstance();

                    /** @var \Magento\Sales\Model\Order $order */
                    $order = $payment->getOrder();

                    if ($payment->getAdditionalInformation(ExpressCheckout::PAYMENT_INFO_TRANSPORT_TOKEN)) {
                        $payment->capture();
                        $stateObject->setState($order->getState());
                        $stateObject->setStatus($order->getStatus());
                        $stateObject->setIsNotified($order->getIsCustomerNotified());
                    } else {
                        $order->setCanSendNewEmailFlag(false);

                        $payment->setAmountAuthorized($order->getTotalDue());
                        $payment->setBaseAmountAuthorized($order->getBaseTotalDue());

                        $stateObject->setState(Order::STATE_PENDING_PAYMENT);
                        $stateObject->setStatus(Order::STATE_PENDING_PAYMENT);
                        $stateObject->setIsNotified(false);
                    }
                    break;
                default:
                    break;
            }

        }
        return $result;
    }
}
