<?php

namespace Tapbuy\Checkout\Model;

class Quote
{
    protected $_configurableProduct;
    protected $_productRepository;

    /**
     * Quote constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     */
    public function __construct(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurableProduct,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_configurableProduct = $configurableProduct;
        $this->_productRepository = $productRepository;
    }

    /**
     * Convert a simple product to a configurable product before adding the product to cart
     *
     * @param $subject
     * @param \Magento\Catalog\Model\Product $product
     * @param null $request
     * @param string $processMode
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeAddProduct($subject, \Magento\Catalog\Model\Product $product, $request = null, $processMode = 'full')
    {
        if ($product->getTypeId() == 'simple') {
            $configurable_products = $this->_configurableProduct->getParentIdsByChild($product->getId());
            $configurable_product = (count($configurable_products) == 1 ? $this->_productRepository->getById($configurable_products[0]) : null);
            if ($configurable_product && $configurable_product->getTypeId() == 'configurable') {
                // replace with configurable product
                $configurableAttributes = $this->_configurableProduct->getConfigurableAttributesAsArray($configurable_product);

                // check for request quantity
                if ($request === null) {
                    $request = 1;
                }
                if (is_numeric($request)) {
                    $request = new \Magento\Framework\DataObject(['qty' => $request]);
                }

                // rebuild super attributes
                $superAttributes = [];
                foreach ($configurableAttributes as $configurableAttribute) {
                    $value = $product->getData($configurableAttribute['attribute_code']);
                    $superAttributes[$configurableAttribute['attribute_id']] = $value;
                }
                $request->addData(['super_attribute' => $superAttributes]);

                // replace current product with configurable product
                $product = $configurable_product;
            }
        }

        return [$product, $request, $processMode];
    }
}
