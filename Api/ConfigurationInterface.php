<?php

namespace Tapbuy\Checkout\Api;

interface ConfigurationInterface
{
    /**
     * Checks module present
     *
     * @api
     * @return boolean
     */
    public function checkModule();

    /**
     * Returns module version
     *
     * @api
     * @return mixed
     */
    public function getVersion();
}
