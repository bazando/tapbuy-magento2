<?php
namespace Tapbuy\Checkout\Api;

interface DummyInterface
{
    /**
     * Dummy interface to test the API
     *
     * @api
     * @param string $dummy
     * @return mixed
     */
    public function DummyData($dummy);
}
