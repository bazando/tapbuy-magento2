<?php

namespace Tapbuy\Checkout\Api;

interface ProductInterface
{
    /**
     * Enables an administrative user to return information for a specified product
     *
     * @param string $sku
     * @return \Tapbuy\Checkout\Api\Data\TapbuyProduct
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProduct($sku);
}
