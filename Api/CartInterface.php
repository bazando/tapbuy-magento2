<?php

namespace Tapbuy\Checkout\Api;

interface CartInterface
{
    /**
     * Enables an administrative user to return information for a specified cart
     *
     * @param string $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCart($cartId);

    /**
     * Returns the maskedId of a cartId
     *
     * @param string $cartId
     * @return string
     */
    public function getCartMaskedId($cartId);

    /**
     * Create a cart (for guest user or logged user)
     *
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createCart();

    /**
     * Create a cart (for guest user or logged user) and add products to it
     *
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createCartWithProducts();

    /**
     * Assign a guest cart to a customer
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setCustomer($cartId);

    /**
     * Assign guest cart products to a customer cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function assignGuestCartProductToCustomerCart($cartId);

    /**
     * Get the active cart of a customer
     *
     * @param int $customerId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCartCustomer($customerId);

    /**
     * Delete a list of products from a cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deleteProducts($cartId);

    /**
     * Add a list of products to a cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addProducts($cartId);

    /**
     * Update a variation of a product in a cart (remove the item and add a new one)
     *
     * @param int $cartId
     * @param int $itemId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateProduct($cartId, $itemId);

    /**
     * Add a coupon
     *
     * @param int $cartId
     * @param string $couponCode
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addCoupon($cartId, $couponCode);

    /**
     * Remove a coupon
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function removeCoupon($cartId);

    /**
     * Set shipping info to a cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setShippingInformation($cartId);

    /**
     * Set billing address to cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setBillingAddress($cartId);

    /**
     * Unlock cart after a failed or canceled payment
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function unlock($cartId);

    /**
     * New version of unlock cart after a failed or canceled payment
     *
     * @param string $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    public function unlockNew($cartId);

    /**
     * Apply customer credit balance to cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function applyBalance($cartId);

    /**
     * Remove customer credit balance from cart
     *
     * @param int $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function removeBalance($cartId);

    /**
     * @param string $cartId
     * @param string $giftCardCode
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function applyGiftCard($cartId, $giftCardCode);

    /**
     * @param string $cartId
     * @param string $giftCardCode
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function removeGiftCard($cartId, $giftCardCode);

    /**
     * Duplicate a cart
     *
     * @param string $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function duplicate($cartId);

    /**
     * Deactivate a cart
     *
     * @param string $cartId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function deactivate($cartId);

    /**
     * Get cart totals of a specified cart
     *
     * @param string $cartId
     * @return \Magento\Quote\Api\Data\TotalsInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCartTotals($cartId);


}
