<?php

namespace Tapbuy\Checkout\Api;

interface AttributeInterface
{

    /**
     * Get a product attribute data
     *
     * @param string $attributeName
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttribute
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function gettAttribute($attributeName);

}
