<?php

namespace Tapbuy\Checkout\Api;

interface CustomerInterface
{
    /**
     * Get customer data
     *
     * @param int $customerId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCustomer
     */
    public function getCustomer($customerId);

    /**
     * Get card aliases for user
     *
     * @param int $customerId
     * @param string $psp
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    public function getCardsAliases($customerId, $psp);

    /**
     * Add a card alias for a user
     *
     * @param int $customerId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    public function addCardAlias($customerId);

    /**
     * Delete a card alias for a user
     *
     * @param int $customerId
     * @param string $cardAliasId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCardAlias[]
     */
    public function deleteCardAlias($customerId, $cardAliasId);

    /**
     * Get customer balance
     *
     * @param int $customerId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCustomerBalance
     */
    public function getCustomerBalance($customerId);
}
