<?php

namespace Tapbuy\Checkout\Api\Data;

use Magento\Catalog\Api\Data\ProductAttributeInterface;

class TapbuyAttribute
{

    /**
     * @var ProductAttributeInterface
     */
    protected $attribute;

    /**
     * @var TapbuyAttributeExtendedOption[]
     */
    protected $extendedOptions = [];

    /**
     * @param ProductAttributeInterface $attribute
     * @param TapbuyAttributeExtendedOption[] $extendedOptions
     */
    public function __construct(ProductAttributeInterface $attribute = null, $extendedOptions = [])
    {
        $this->attribute = $attribute;
        $this->extendedOptions = $extendedOptions;
    }

    /**
     * Getter for $attribute
     *
     * @return \Magento\Catalog\Api\Data\ProductAttributeInterface
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Setter for $attribute
     *
     * @param ProductAttributeInterface $attribute
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
        return $this;
    }

    /**
     * Getter for $extendedOptions
     *
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption[]
     */
    public function getExtendedOptions()
    {
        return $this->extendedOptions;
    }

    /**
     * Setter for $extendedOptions
     *
     * @param TapbuyAttributeExtendedOption[] $extendedOptions
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttribute
     */
    public function setExtendedOptions($extendedOptions)
    {
        $this->extendedOptions = $extendedOptions;
        return $this;
    }

    /**
     * Add an extended option
     *
     * @param TapbuyAttributeExtendedOption $extendedOption
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttribute
     */
    public function addExtendedOption(TapbuyAttributeExtendedOption $extendedOption)
    {
        $this->extendedOptions[] = $extendedOption;
        return $this;
    }
}
