<?php

namespace Tapbuy\Checkout\Api\Data;

class TapbuyCustomer
{
    /**
     * @var \Magento\Customer\Api\Data\CustomerInterface
     */
    protected $customer;

    /**
     * @var \Tapbuy\Checkout\Api\Data\TapbuyCustomerBalance
     */
    protected $creditBalance;

    /**
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param \Magento\Customer\Api\Data\CustomerInterface $customer
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCustomer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCustomerBalance
     */
    public function getCreditBalance()
    {
        return $this->creditBalance;
    }

    /**
     * @param \Tapbuy\Checkout\Api\Data\TapbuyCustomerBalance $creditBalance
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCustomer
     */
    public function setCreditBalance($creditBalance)
    {
        $this->creditBalance = $creditBalance;
        return $this;
    }

}
