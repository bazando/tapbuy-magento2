<?php

namespace Tapbuy\Checkout\Api\Data;

use Magento\Catalog\Api\Data\ProductInterface;

class TapbuyProduct
{
    /**
     * @var ProductInterface
     */
    protected $product = null;

    /**
     * @var ProductInterface[]
     */
    protected $childrenProducts = [];

    /**
     * @var ProductInterface
     */
    protected $parentProduct = null;

    /**
     * @var string
     */
    protected $coverImageUrl = null;

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param ProductInterface $product
     * @return $this
     */
    public function setProduct(ProductInterface $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    public function getChildrenProducts()
    {
        return $this->childrenProducts;
    }

    /**
     * @param ProductInterface[] $childrenProducts
     * @return $this
     */
    public function setChildrenProducts($childrenProducts)
    {
        $this->childrenProducts = $childrenProducts;
        return $this;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getParentProduct()
    {
        return $this->parentProduct;
    }

    /**
     * @param ProductInterface $parentProduct
     * @return $this
     */
    public function setParentProduct($parentProduct)
    {
        $this->parentProduct = $parentProduct;
        return $this;
    }

    /**
     * @return string
     */
    public function getCoverImageUrl()
    {
        return $this->coverImageUrl;
    }

    /**
     * @param string $coverImageUrl
     * @return $this
     */
    public function setCoverImageUrl(string $coverImageUrl)
    {
        $this->coverImageUrl = $coverImageUrl;
        return $this;
    }

}
