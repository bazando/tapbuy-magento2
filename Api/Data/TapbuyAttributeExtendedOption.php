<?php

namespace Tapbuy\Checkout\Api\Data;

class TapbuyAttributeExtendedOption
{

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $imagePath;

    /**
     * @var string
     */
    protected $imageThumb;

    /**
     * @var string
     */
    protected $image;

    /**
     * @var string
     */
    protected $hexCode;

    public function __construct($label = null, $value = null, $imagePath = null, $imageThumb = null, $image = null, $hexCode = null)
    {
        $this->label = $label;
        $this->value = $value;
        $this->imagePath = $imagePath;
        $this->imageThumb = $imageThumb;
        $this->image = $image;
        $this->hexCode = $hexCode;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageThumb()
    {
        return $this->imageThumb;
    }

    /**
     * @param string $imageThumb
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption
     */
    public function setImageThumb($imageThumb)
    {
        $this->imageThumb = $imageThumb;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getHexCode()
    {
        return $this->hexCode;
    }

    /**
     * @param string $hexCode
     * @return \Tapbuy\Checkout\Api\Data\TapbuyAttributeExtendedOption
     */
    public function setHexCode($hexCode)
    {
        $this->hexCode = $hexCode;
        return $this;
    }
}
