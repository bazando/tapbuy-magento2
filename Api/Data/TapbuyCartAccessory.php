<?php

namespace Tapbuy\Checkout\Api\Data;

class TapbuyCartAccessory
{
    /**
     * @var int|null
     */
    protected $id = null;

    /**
     * @var string|null
     */
    protected $name = null;

    /**
     * @var string|null
     */
    protected $image = null;

    /**
     * @var float|null
     */
    protected $price = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }
}
