<?php

namespace Tapbuy\Checkout\Api\Data;

class TapbuyCustomerBalance
{
    /**
     * @var int
     */
    protected $customerId;

    /**
     * @var int
     */
    protected $websiteId;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $baseCurrencyCode;

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param int $customeId
     * @return TapbuyCustomerBalance
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return int
     */
    public function getWebsiteId()
    {
        return $this->websiteId;
    }

    /**
     * @param int $websiteId
     * @return TapbuyCustomerBalance
     */
    public function setWebsiteId($websiteId)
    {
        $this->websiteId = $websiteId;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return TapbuyCustomerBalance
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getBaseCurrencyCode()
    {
        return $this->baseCurrencyCode;
    }

    /**
     * @param string $baseCurrencyCode
     * @return TapbuyCustomerBalance
     */
    public function setBaseCurrencyCode($baseCurrencyCode)
    {
        $this->baseCurrencyCode = $baseCurrencyCode;
        return $this;
    }

}
