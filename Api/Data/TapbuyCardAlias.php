<?php

namespace Tapbuy\Checkout\Api\Data;

class TapbuyCardAlias
{

    /**
     * Card alias ID in DB
     * @var string
     */
    protected $aliasId = null;

    /**
     * ID of the customer
     * @var string
     */
    protected $customerId = null;

    /**
     * Name of the alias
     * @var string
     */
    protected $aliasName = null;

    /**
     * Type of card (visa, master, amex , etc..)
     * @var string
     */
    protected $cardType = null;

    /**
     * Month of the card expiration date
     * @var int
     */
    protected $cardExpMonth = null;

    /**
     * Year of the card expiration date
     * @var int
     */
    protected $cardExpYear = null;

    /**
     * Name on the card
     * @var string
     */
    protected $cardName = null;

    /**
     * The last 4 digit of the card
     * @var int
     */
    protected $cardLast4Digit = null;

    /**
     * Card number which has been encoded (ex: 411111xxxxxx1111)
     * @var string
     */
    protected $cardEncodedNumber = null;

    /**
     * Card token
     * @var string
     */
    protected $cardToken = null;

    /**
     * pspResponse
     * @var string
     */
    protected $pspResponse = null;

    /**
     * @param string $aliasId
     * @param string $customerId
     * @param string $aliasName
     * @param string $cardType
     * @param int $cardExpMonth
     * @param int $cardExpYear
     * @param string $cardName
     * @param int $cardLast4Digit
     * @param string $cardEncodedNumber
     * @param string $cardToken
     * @param string $pspResponse
     */
    public function __construct($aliasId = null, $customerId = null, $aliasName = null, $cardType = null, $cardExpMonth = null, $cardExpYear = null, $cardName = null, $cardLast4Digit = null, $cardEncodedNumber = null, $cardToken = null, $pspResponse = null)
    {
        $this->aliasId = $aliasId;
        $this->customerId = $customerId;
        $this->aliasName = $aliasName;
        $this->cardType = $cardType;
        $this->cardExpMonth = $cardExpMonth;
        $this->cardExpYear = $cardExpYear;
        $this->cardName = $cardName;
        $this->cardLast4Digit = $cardLast4Digit;
        $this->cardEncodedNumber = $cardEncodedNumber;
        $this->cardToken = $cardToken;
        $this->pspResponse = $pspResponse;
    }

    /**
     * @return string
     */
    public function getAliasId()
    {
        return $this->aliasId;
    }

    /**
     * @param string $aliasId
     * @return TapbuyCardAlias
     */
    public function setAliasId($aliasId)
    {
        $this->aliasId = $aliasId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param string $customerId
     * @return TapbuyCardAlias
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAliasName()
    {
        return $this->aliasName;
    }

    /**
     * @param string $aliasName
     * @return TapbuyCardAlias
     */
    public function setAliasName($aliasName)
    {
        $this->aliasName = $aliasName;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param string $cardType
     * @return TapbuyCardAlias
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
        return $this;
    }

    /**
     * @return int
     */
    public function getCardExpMonth()
    {
        return $this->cardExpMonth;
    }

    /**
     * @param int $cardExpMonth
     * @return TapbuyCardAlias
     */
    public function setCardExpMonth($cardExpMonth)
    {
        $this->cardExpMonth = $cardExpMonth;
        return $this;
    }

    /**
     * @return int
     */
    public function getCardExpYear()
    {
        return $this->cardExpYear;
    }

    /**
     * @param int $cardExpYear
     * @return TapbuyCardAlias
     */
    public function setCardExpYear($cardExpYear)
    {
        $this->cardExpYear = $cardExpYear;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardName()
    {
        return $this->cardName;
    }

    /**
     * @param string $cardName
     * @return TapbuyCardAlias
     */
    public function setCardName($cardName)
    {
        $this->cardName = $cardName;
        return $this;
    }

    /**
     * @return int
     */
    public function getCardLast4Digit()
    {
        return $this->cardLast4Digit;
    }

    /**
     * @param int $cardLast4Digit
     * @return TapbuyCardAlias
     */
    public function setCardLast4Digit($cardLast4Digit)
    {
        $this->cardLast4Digit = $cardLast4Digit;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardEncodedNumber()
    {
        return $this->cardEncodedNumber;
    }

    /**
     * @param string $cardEncodedNumber
     * @return TapbuyCardAlias
     */
    public function setCardEncodedNumber($cardEncodedNumber)
    {
        $this->cardEncodedNumber = $cardEncodedNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getCardToken()
    {
        return $this->cardToken;
    }

    /**
     * @param string $cardToken
     * @return TapbuyCardAlias
     */
    public function setCardToken($cardToken)
    {
        $this->cardToken = $cardToken;
        return $this;
    }

    /**
     * @return string
     */
    public function getPspResponse()
    {
        return $this->pspResponse;
    }

    /**
     * @param string $pspResponse
     * @return TapbuyCardAlias
     */
    public function setPspResponse($pspResponse)
    {
        $this->pspResponse = $pspResponse;
        return $this;
    }

}
