<?php

namespace Tapbuy\Checkout\Api\Data;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Api\Data\TotalsInterface;

class TapbuyCart
{

    /**
     * @var \Magento\Quote\Api\Data\CartInterface
     */
    protected $cart = null;

    /**
     * @var \Magento\Quote\Api\Data\TotalsInterface
     */
    protected $cartTotals = null;

    /**
     * @var \Magento\Quote\Api\Data\PaymentMethodInterface[]
     */
    protected $cartPaymentMethods = [];

    /**
     * @var string
     */
    protected $maskedId = null;

    /**
     * @var bool
     */
    protected $hasRecurring = false;

    /**
     * @var bool
     */
    protected $hasOnlyRecurring = false;

    /**
     * @var bool
     */
    protected $hasGiftProduct = false;

    /**
     * @var \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories
     */
    protected $accessories = null;

    /**
     * @return \Magento\Quote\Api\Data\CartInterface
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param \Magento\Quote\Api\Data\CartInterface $cart
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setCart(CartInterface $cart)
    {
        $this->cart = $cart;
        return $this;
    }

    /**
     * @return \Magento\Quote\Api\Data\TotalsInterface
     */
    public function getCartTotals()
    {
        return $this->cartTotals;
    }

    /**
     * @param \Magento\Quote\Api\Data\TotalsInterface $cartTotals
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setCartTotals(TotalsInterface $cartTotals)
    {
        $this->cartTotals = $cartTotals;
        return $this;
    }

    /**
     * @return \Magento\Quote\Api\Data\PaymentMethodInterface[]
     */
    public function getCartPaymentMethods()
    {
        return $this->cartPaymentMethods;
    }

    /**
     * @param \Magento\Quote\Api\Data\PaymentMethodInterface[] $cartPaymentMethods
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setCartPaymentMethods($cartPaymentMethods)
    {
        $this->cartPaymentMethods = $cartPaymentMethods;
        return $this;
    }

    /**
     * @return string
     */
    public function getMaskedId()
    {
        return $this->maskedId;
    }

    /**
     * @param string $maskedId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setMaskedId($maskedId)
    {
        $this->maskedId = $maskedId;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasRecurring()
    {
        return $this->hasRecurring;
    }

    /**
     * @param bool $hasRecurring
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setHasRecurring($hasRecurring)
    {
        $this->hasRecurring = $hasRecurring;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasOnlyRecurring()
    {
        return $this->hasOnlyRecurring;
    }

    /**
     * @param bool $hasOnlyRecurring
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setHasOnlyRecurring($hasOnlyRecurring)
    {
        $this->hasOnlyRecurring = $hasOnlyRecurring;
        return $this;
    }

    /**
     * @return bool
     */
    public function getHasGiftProduct()
    {
        return $this->hasGiftProduct;
    }

    /**
     * @param bool $hasGiftProduct
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setHasGiftProduct($hasGiftProduct)
    {
        $this->hasGiftProduct = $hasGiftProduct;
        return $this;
    }

    /**
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories
     */
    public function getAccessories()
    {
        return $this->accessories;
    }

    /**
     * @param \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories $accessories
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     */
    public function setAccessories(TapbuyCartAccessories $accessories)
    {
        $this->accessories = $accessories;
        return $this;
    }
}
