<?php

namespace Tapbuy\Checkout\Api\Data;

interface CartDetails extends \Magento\Quote\Api\Data\CartInterface
{
    const KEY_COUPON_CODE = 'coupon_code';
    const KEY_GRAND_TOTAL = 'grand_total';
    const KEY_SUBTOTAL = 'subtotal';
    const KEY_SUBTOTAL_WITH_DISCOUNT = 'subtotal_with_discount';
    const KEY_QUOTE_CURRENCY_CODE = 'quote_currency_code';

    /**
     * Returns coupon code if any
     *
     * @return string|null Coupon code
     */
    public function getCouponCode();

    /**
     * Returns subtotal
     *
     * @return float|null total
     */
    public function getSubtotal();

    /**
     * Returns subtotal with discount
     *
     * @return float|null total
     */
    public function getSubtotalWithDiscount();

    /**
     * Returns grand total
     *
     * @return float|null total
     */
    public function getGrandTotal();

    /**
     * Returns currency code
     *
     * @return string|null total
     */
    public function getQuoteCurrencyCode();
}
