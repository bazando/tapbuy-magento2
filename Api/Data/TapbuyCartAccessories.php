<?php

namespace Tapbuy\Checkout\Api\Data;

class TapbuyCartAccessories
{

    /**
     * @var string|null
     */
    protected $label = null;

    /**
     * @var string|null
     */
    protected $description = null;

    /**
     * @var \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory[]
     */
    protected $items = [];

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param \Tapbuy\Checkout\Api\Data\TapbuyCartAccessory[] $items
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCartAccessories
     */
    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }
}
