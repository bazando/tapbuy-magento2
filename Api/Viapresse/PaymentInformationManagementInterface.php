<?php

namespace Tapbuy\Checkout\Api\Viapresse;

/**
 * Interface for managing quote payment information
 * @api
 */
interface PaymentInformationManagementInterface
{
    /**
     * Lists available payment methods for a specified shopping cart.
     *
     * @param int $cartId The cart ID.
     * @return \Magento\Quote\Api\Data\PaymentMethodInterface[] Array of payment methods.
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     */
    public function getList($cartId);
}
