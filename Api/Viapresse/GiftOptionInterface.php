<?php

namespace Tapbuy\Checkout\Api\Viapresse;

interface GiftOptionInterface
{
    /**
     * Update gift options
     * @param string $cartId
     * @param string $itemId
     * @return \Tapbuy\Checkout\Api\Data\TapbuyCart
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function update($cartId, $itemId);
}
