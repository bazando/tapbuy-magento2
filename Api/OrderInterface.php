<?php

namespace Tapbuy\Checkout\Api;

use Magento\Sales\Api\Data\OrderStatusHistoryInterface;

interface OrderInterface
{

    /**
     * Add a comment to an order, the set status is optional
     *
     * @param int $orderId
     * @param OrderStatusHistoryInterface $statusHistory
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function addComment($orderId, OrderStatusHistoryInterface $statusHistory);

    /**
     * Manage the return of Paypal Express Checkout
     *
     * @param string $orderExtId
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function expressCheckoutReturn($orderExtId);

}
